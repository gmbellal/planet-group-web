@extends('homepage.layout.master')

@section('extra_css')

@endsection

@section('content')
	   <!-- about breadcrumb -->
      <section class="w3l-about-breadcrumb text-left">
         <div class="breadcrumb-bg breadcrumb-bg-about py-sm-5 py-4">
            <div class="container">
               <h2 class="title">Contact</h2>
               <ul class="breadcrumbs-custom-path mt-2">
                  <li><a href="{{url('home')}}">Home</a></li>
                  <li class="active"><span class="fa fa-arrow-right mx-2" aria-hidden="true"></span> Contact </li>
               </ul>
            </div>
         </div>
      </section>
      <!-- //about breadcrumb -->

      <!-- /contact-->
      <div class="contact-form py-5" id="contact">
         <div class="container py-lg-5 py-md-4">
            <h3 class="hny-title mb-lg-5 mb-4">Get in touch</h3>
            <!-- validation error -->
				@include('dashboard.layout.common.validation_error')
            <div class="contacts12-main mb-5">
               <form class="form-horizontal" action="{{url('contact')}}"  method="post" enctype="multipart/form-data" >
               {{ csrf_field() }}
                  <div class="main-input">
                     <div class="d-grid">
                        <input type="text" name="name" id="w3lName" value="{{old('name')}}" placeholder="Your Name" class="contact-input" />
                        <input type="tel" name="phone_number" id="w3lPhone" value="{{old('phone_number')}}" placeholder="Phone Number"
                           class="contact-input" />
                     </div>
                     <div class="d-grid">
                        <input type="email" name="email" id="w3lSender" value="{{old('email')}}" placeholder="Your Email id"
                           class="contact-input" required />
                        <input type="text" name="subject" id="w3lSubject" value="{{old('subject')}}" placeholder="Subject"
                           class="contact-input" />
                     </div>
                  </div>
                  <textarea class="contact-textarea" name="message" id="w3lMessage"
                     placeholder="Type your message here" required>{{old('message')}}</textarea>
                  <div class="text-right">
                     <button class="btn btn-style btn-secondary btn-contact">Submit Now</button>
                  </div>
               </form>
            </div>
            <div class="row pt-lg-4">
               <div class="col-md-12">
                  <div class="row">
                     <div class="col-lg-4 col-md-6">
                        <div class="contact-address" style="min-height: 134px;">
                           <ul>
                              <li class="icon-color"><span class="fa fa-map-marker"></span>
                                <span>{{!empty($contact->address)? $contact->address:''}}</span> 
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 mt-md-0 mt-4">
                        <div class="contact-address" style="min-height: 105px;">
                           <ul>
                              <li class="icon-color">
                                 <span class="fa fa-phone"></span> <span>
                                 <a href="tel:{{!empty($contact->phone)? $contact->phone:''}}" style="margin-top: 12px;">Phone : {{!empty($contact->phone)? $contact->phone:''}}</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-4 col-md-6 mt-lg-0 mt-4">
                        <div class="contact-address" style="min-height: 105px;">
                           <ul>
                              <li class="icon-color">
                                 <span class="fa fa-mail-reply"></span><span>
                                 <a href="mailto:{{!empty($contact->email)? $contact->email:''}}" style="margin-top: 12px;"> {{!empty($contact->email)? $contact->email:''}}</a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--//contact-->
  
@endsection()



@section('extra_js')

@endsection()
