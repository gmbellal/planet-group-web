@extends('homepage.layout.master')

@section('extra_css')

@endsection

@section('content')
	  <!-- main-slider -->
	  <section class="w3l-main-slider" id="home">
    <div class="companies20-content">
      <div class="owl-one owl-carousel owl-theme">
        @foreach($slider as $list)
          <div class="item">
            <li>
              <div  title="{{$list->meta}}" class="slider-info" style="background: url({{asset('uploads/'.$list->image)}}) no-repeat center;background-size: cover;min-height: 60vh;position: relative;z-index: 0;display: grid;align-items: center; margin-top: 90px;">
                <div class="banner-info">
                  <div class="container">
                    <div class="banner-info-bg text-left">
                      <h5>{{$list->title}}</h5>
                      <a href="{{$list->url}}" class="btn btn-white">Read More</a>
                    </div>
                  </div>
                </div>
                <div class="banhny-w3grids" meta>
                  @if($list->sub_title_1 && $list->sub_image_1)
                  <div class="banhny-w3grids-1">
                    <img src="{{asset('uploads/'.$list->sub_image_1)}}" class="img-curve img-fluid" alt="" />
                    <h4><a href="{{$list->sub_url_1}}" class="hnys-title">{{$list->sub_title_1}}</a></h4>
                  </div>
                  @endif
                  @if($list->sub_title_2 && $list->sub_image_2)
                  <div class="banhny-w3grids-1">
                    <img src="{{asset('uploads/'.$list->sub_image_2)}}" class="img-curve img-fluid" alt="" />
                    <h4><a href="{{$list->sub_url_2}}" class="hnys-title">{{$list->sub_title_2}}</a></h4>
                  </div>
                  @endif
                </div>
              </div>
            </li>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- /main-slider -->

  <!--/grids-->
  <section class="w3l-grids-3 py-5" id="about">
    <div class="container py-md-5">
      <div class="row bottom-ab-grids align-items-center">
        <div class="col-lg-6 bottom-ab-left pr-lg-5">
          <h6 class="sub-title">About Us</h6>
          <h3 class="hny-title">{{!empty($about->title)? $about->title:''}}</h3>
          <p class="my-3 pr-lg-4" style="text-align: justify;">{{!empty($about->body)? str_limit(strip_tags($about->body), $limit = 400, $end = ' . . .'):''}}</p>
          <a href="{{!empty($about->url)? $about->url:'#'}}" class="btn btn-style btn-secondary mt-4">Read More</a>
        </div>
        <div class="col-lg-6 bottom-ab-right mt-lg-0 mt-5">
          <img src="{{!empty($about->image)? asset('uploads/'.$about->image):''}}" class="img-curve img-fluid" alt="" />
        </div>
      </div>
    </div>
  </section>
  <!--//grids-->

  <!--/services-->
  <section class="w3l-services1">
    <div id="content-with-photo4-block" class="py-5">
      <div class="container py-md-5">
        <div class="cwp4-two row">
          <div class="cwp4-image col-lg-6 pr-lg-5 mb-lg-0 mb-5">
            <img src="{{!empty($services->image)? asset('uploads/'.$services->image):''}}" class="img-fluid" alt="" />
          </div>
          <div class="cwp4-text col-lg-6">
            <h6 class="sub-title">What We Do</h6>
            <h3 class="hny-title">{{!empty($services->title)? $services->title:''}}</h3>
            <p style="text-align: justify;">{{!empty($services->body)? $services->body:''}}</p>
            <?php
                if(!empty($services->services)){
                  $list = explode(PHP_EOL, $services->services);
                }else{
                  $list = [];
                }
            ?>
            <ul class="cont-4">
              @foreach($list as $value)
                <li><span class="fa fa-check"></span> {{$value}}</li>
              @endforeach
            </ul>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!--//services-->



  <!-- partners -->
  <section class="w3l-clients" id="clients">
    <!-- /grids -->
    <div class="cusrtomer-layout py-1">
      <div class="container py-lg-4 py-md-3 pb-lg-0">
        <div class="heading text-center mx-auto">
          <h3 class="hny-title mb-md-5 mb-4">Our Partners</h3>
        </div>
        <!-- /grids -->
        <div class="testimonial-width mt-5">
          <div id="owl-partner" class="owl-two owl-carousel owl-theme">
            @foreach($partners as $list)
              <div class="item" style="border: none">
                <div class="testimonial-content">
                  <div class="testimonial" style="padding: 30px !important;">
                    <img src="{{asset('uploads/'.$list->image)}}" class="img-fluid" alt="client-img">
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      <!-- /grids -->
    </div>
    <!-- //grids -->
  </section>
  <!-- //partners -->



  <!--/Features-->
  <section class="w3l-how-grids-3 py-1" id="how">
    <div class="container py-md-1">
      <div class="w3l-header mb-md-5 mb-4">
        <h6 class="sub-title">Features</h6>
        <h3 class="hny-title">We Provide High-Quality  Products</h3>
        <!-- <p class="">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p> -->
      </div>
      <div class="row bottom-ab-grids align-items-center">
        @foreach($features as $list)
          <div class="col-lg-3 bottom-ab-left" style="min-height: 450px;">
            <div class="grdhny-info">
              <img src="{{asset('uploads/'.$list->image)}}" style="border: 1px solid #a4a4a4;" class="img-curve img-fluid" alt="{{$list->meta}}" />
              <h4><a href="{{$list->url}}" class="hnys-title">{{$list->title}}</a></h4>
              <p class="mt-2" style="text-align: justify">{{str_limit($list->body, $limit = 240, $end = ' . . .')}}</p>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!--//Features-->


  <!-- achievements -->
  <section class="w3l-stats py-5" id="stats">
    <div class="gallery-inner container py-lg-0 py-3">
      <div class="row stats-con pb-lg-3">
        @foreach($achievements as $list)
          <div class="col-lg-3 col-6 stats_info counter_grid">
            <p class="counter">{{$list->total}}</p>
            <h4>{{$list->title}}</h4>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- //achievements -->



  <!--/w3l-bottom-->
  <section class="w3l-bottom">
    <div class="container py-md-4 py-3 text-center">
      <div class="row my-lg-4 mt-4">
        <div class="col-lg-9 col-md-10 ml-auto">
          <div class="subscribe ml-auto">
            <div class="header-section text-left">
              <h3 class="hny-title two">Get Update From
                Anywhere</h3>
              <h4 class="my-2"> Don't Miss Out On The Good News!</h4>
            </div>
            <form action="{{url('/home')}}" method="post" class="subscribe-wthree pt-lg-3 mt-4">
              {{ csrf_field() }}
              <div class="flex-wrap subscribe-wthree-field">
                <input class="form-control" type="email" placeholder="Enter your email..." name="email" required="">
                <button class="btn btn-style btn-primary" type="submit">Subscribe</button>
              </div>
              <p class="mt-3">By Subscribing this form, you agree to the <a href="#">privacy policy</a> and <a
                  href="#">terms of use</a></p>
            </form>

          </div>
        </div>
      </div>
    </div>
  </section>
  <!--//w3l-bottom-->

  

  <!-- testimonials -->
  <section class="w3l-clients" id="clients">
    <!-- /grids -->
    <div class="cusrtomer-layout py-5">
      <div class="container py-lg-4 py-md-3 pb-lg-0">
        <div class="heading text-center mx-auto">
          <h6 class="sub-title text-center">Here’s what they have to say</h6>
          <h3 class="hny-title mb-md-5 mb-4">our clients do the talking</h3>
        </div>
        <!-- /grids -->
        <div class="testimonial-width mt-5">
          <div id="owl-testimonial" class="owl-two owl-carousel owl-theme">
            @foreach($testimonials as $list)
              <div class="item">
                <div class="testimonial-content">
                  <div class="testimonial" style="padding: 25px">
                    <blockquote style="min-height: 110px;">
                      <span class="fa fa-quote-left" aria-hidden="true"></span>
                      {{str_limit($list->body, $limit = 100, $end = ' . . .')}}
                    </blockquote>
                    <div class="testi-des">
                      <div class="test-img"><img src="{{asset('uploads/'.$list->image)}}" class="img-fluid" alt="client-img">
                      </div>
                      <div class="peopl align-self">
                        <h3>{{$list->client_name}}</h3>
                        <p class="indentity">Example City</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      <!-- /grids -->
    </div>
    <!-- //grids -->
  </section>
  <!-- //testimonials -->
  
@endsection()



@section('extra_js')

@endsection()
