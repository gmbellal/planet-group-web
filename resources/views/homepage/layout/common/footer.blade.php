<?php
  use App\Model\ContactUs;
  use App\Model\Services;
  use App\Model\AboutUs;
  use App\Model\SocialMedia;

  $contact = ContactUs::get()->first();
  $service_list = Services::get()->first();
  $about_footer = AboutUs::get()->first();
  $social_media = SocialMedia::get()->first();
?>



<!--/w3l-footer-29-main-->
<section class="w3l-footer-29-main">
    <div class="footer-29 py-5">
      <div class="container py-lg-4">
        <div class="row footer-top-29">
          <div class="footer-list-29 col-lg-4">
            <h6 class="footer-title-29">About Us</h6>
            <p class="pr-lg-5">{{!empty($about_footer->body)? str_limit(strip_tags($about_footer->body), $limit = 140, $end = ' . . . '):''}} <a href="{{!empty($about_footer->url)? $about_footer->url : '#'}}" style="color: #28a745;">Read more</a></p>
            <div class="main-social-footer-29 mt-4">
              <a href="{{!empty($social_media->facebook)? $social_media->facebook:'#facebook'}}" class="facebook"><span class="fa fa-facebook"></span></a>
              <a href="{{!empty($social_media->twitter)? $social_media->twitter:'#twitter'}}" class="twitter"><span class="fa fa-twitter"></span></a>
              <a href="{{!empty($social_media->instagram)? $social_media->instagram:'#instagram'}}" class="instagram"><span class="fa fa-instagram"></span></a>
              <a href="{{!empty($social_media->linkedin)? $social_media->linkedin:'#linkedin'}}" class="linkedin"><span class="fa fa-linkedin"></span></a>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 col-sm-4 footer-list-29 footer-2 mt-lg-0 mt-5">

            <ul>
              <h6 class="footer-title-29">Useful Links</h6>
              <li><a href="{{url('/home')}}">Home</a></li>
              <li><a href="{{url('/about')}}">About</a></li>
              <!-- <li><a href="services.html">Services</a></li>
              <li><a href="#"> Blog posts</a></li> -->
              <li><a href="{{url('/contact')}}">Contact us</a></li>
            </ul>
          </div>

            <?php
              if(!empty($service_list->services)){
                $list_footer = explode(PHP_EOL, $service_list->services);
              }else{
                $list_footer = [];
              }
            ?>
          <div class="col-lg-3 col-md-6 footer-list-29 footer-3 mt-lg-0 mt-5">
            <ul>
              <h6 class="footer-title-29">Services</h6>
              @foreach($list_footer as $value)
                <li><a href="#">{{$value}}</a></li>
              @endforeach
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 col-sm-8 footer-list-29 footer-1 mt-lg-0 mt-5">
            <h6 class="footer-title-29">Contact Us</h6>
            <ul>
              <li>
                <p><span class="fa fa-map-marker"></span>{{!empty($contact->address)? $contact->address:''}}</p>
              </li>
              <li><a href="tel:{{!empty($contact->phone)? $contact->phone:''}}"><span class="fa fa-phone"></span>{{!empty($contact->phone)? $contact->phone:''}}</a></li>
              <li><a href="mailto:{{!empty($contact->email)? $contact->email:''}}" class="mail"><span class="fa fa-envelope-open-o"></span>{{!empty($contact->email)? $contact->email:''}}</a></li>
            </ul>
          </div>

        </div>
      </div>
    </div>
  </section>

  <section class="w3l-footer-29-main w3l-copyright">
    <div class="container">
      <div class="bottom-copies">
        <p class="copy-footer-29 text-center">© 2022 Planet Group. All rights reserved. Developed by <a
            href="https://www.planet.com.bd" target="_blank">
            Planet Group</a></p>
      </div>
    </div>

    <!-- move top -->
    <button onclick="topFunction()" id="movetop" title="Go to top">
      <span class="fa fa-angle-up"></span>
    </button>
    <script>
      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function () {
        scrollFunction()
      };

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          document.getElementById("movetop").style.display = "block";
        } else {
          document.getElementById("movetop").style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
    </script>
    <!-- /move top -->
</section>
<!-- //footer-29 block -->