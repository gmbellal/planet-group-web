<?php
	use App\Model\Companies;
	$com_list = Companies::orderBy('sequence','asc')->where('status','Active')->get();
?>


<!--header-->
<header id="site-header" class="fixed-top">
    <div class="container">
      <nav class="navbar navbar-expand-lg stroke">
        <a class="navbar-brand" href="{{url('home')}}">
            <img src="{{asset('assets/homepage/images/brand-logo.png')}}" alt="Your logo" title="Your logo" style="height:80px; width: auto" />
        </a>
        <h1><a class="navbar-brand mr-lg-5" href="{{url('home')}}">PLANET<span> GROUP</span></a></h1>

        <button class="navbar-toggler  collapsed bg-gradient" type="button" data-toggle="collapse"
          data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
          aria-label="Toggle navigation">
          <span class="navbar-toggler-icon fa icon-expand fa-bars"></span>
          <span class="navbar-toggler-icon fa icon-close fa-times"></span>
          </span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item  {{str_contains(Request::path(), 'home') ? 'active' : '' }}">
              <a class="nav-link" href="{{url('home')}}">Home</a>
            </li>
            <li class="nav-item  {{str_contains(Request::path(), 'about') ? 'active' : '' }}">
              <a class="nav-link" href="{{url('about')}}">About</a>
            </li>


            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Company <span class="fa fa-angle-down"></span>
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                @foreach($com_list as $list)
                  <a class="dropdown-item scroll" href="{{url('company').'/'.$list->url}}">{{$list->company_name}}</a>
                @endforeach
              </div>
            </li>


            <!-- <li class="nav-item">
              <a class="nav-link" href="services.html">Services</a>
            </li> -->
          
            <li class="nav-item  {{str_contains(Request::path(), 'contact') ? 'active' : '' }}">
              <a class="nav-link" href="{{url('contact')}}">Contact</a>
            </li>

          </ul>
        </div>
        <!-- <div class="d-lg-block d-none">
          <a href="contact.html" class="btn btn-style btn-secondary ml-lg-3">Free Quote</a>
        </div> -->
        <!-- toggle switch for light and dark theme -->
        <div class="mobile-position">
          <nav class="navigation">
            <div class="theme-switch-wrapper">
              <label class="theme-switch" for="checkbox">
                <input type="checkbox" id="checkbox">
                <div class="mode-container">
                  <i class="gg-sun"></i>
                  <i class="gg-moon"></i>
                </div>
              </label>
            </div>
          </nav>
        </div>
        <!-- //toggle switch for light and dark theme -->
      </nav>
    </div>
</header>
<!-- //header -->