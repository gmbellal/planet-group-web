<!doctype html>
<html lang="zxx">

	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>{{$title}}</title>
		<link href="//fonts.googleapis.com/css2?family=Poppins:wght@300;600;700&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('assets/homepage/css/style-starter.css')}}">
		@yield('extra_css')
	</head>

	<body>

		@include('homepage.layout.common.header')
		
		@yield('content')

		@include('homepage.layout.common.footer')


		<script src="{{asset('assets/homepage/js/jquery-3.3.1.min.js')}}"></script>
		<script src="{{asset('assets/homepage/js/theme-change.js')}}"></script>
		<script src="{{asset('assets/homepage/js/owl.carousel.js')}}"></script>
		<script src="{{asset('assets/homepage/js/jquery.waypoints.min.js')}}"></script>
		<script src="{{asset('assets/homepage/js/jquery.countup.js')}}"></script>
		<script src="{{asset('assets/homepage/js/bootstrap.min.js')}}"></script>


		@yield('extra_js')
		
		<script>
			$(function () {
				$('.navbar-toggler').click(function () {
					$('body').toggleClass('noscroll');
				})
			});
		</script>

		<script>
			$(document).ready(function () {
			$('.owl-one').owlCarousel({
				loop: true,
				margin: 0,
				nav: false,
				responsiveClass: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 1000,
				autoplayHoverPause: false,
				responsive: {
				0: {
					items: 1,
					nav: false
				},
				480: {
					items: 1,
					nav: false
				},
				667: {
					items: 1,
					nav: true
				},
				1000: {
					items: 1,
					nav: true
				}
				}
			})
			})
		</script>
	<!-- //script -->
	
	<!-- script for tesimonials carousel slider -->
	<script>
		$(document).ready(function () {


			$("#owl-partner").owlCarousel({
				loop: true,
				margin: 10,
				nav: false,
				responsiveClass: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 1000,
				autoplayHoverPause: false,
				
				responsive: {
					0: {
						items: 1,
						nav: false
					},
					250: {
						items: 2,
						nav: false
					},
					500: {
						items: 3,
						nav: false
					},
					750: {
						items: 4,
						nav: false
					},
					1000: {
						items: 5,
						nav: false,
						loop: true
					}
				}
			});



			$("#owl-testimonial").owlCarousel({
				loop: true,
				margin: 20,
				nav: false,
				responsiveClass: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 1000,
				autoplayHoverPause: false,
				responsive: {
					0: {
						items: 1,
						nav: false
					},
					736: {
						items: 2,
						nav: false
					},
					1000: {
						items: 3,
						nav: false,
						loop: true
					}
				}
			});



		});
	</script>
	<!-- //script for tesimonials carousel slider -->
	<!-- stats number counter-->
	
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats number counter -->
	<!--/MENU-JS-->
	<script>
		$(window).on("scroll", function () {
		var scroll = $(window).scrollTop();

		if (scroll >= 80) {
			$("#site-header").addClass("nav-fixed");
		} else {
			$("#site-header").removeClass("nav-fixed");
		}
		});

		//Main navigation Active Class Add Remove
		$(".navbar-toggler").on("click", function () {
		$("header").toggleClass("active");
		});
		$(document).on("ready", function () {
		if ($(window).width() > 991) {
			$("header").removeClass("active");
		}
		$(window).on("resize", function () {
			if ($(window).width() > 991) {
			$("header").removeClass("active");
			}
		});
		});
	</script>
	<!--//MENU-JS-->


	</body>

</html>