@extends('homepage.layout.master')

@section('extra_css')
<style>
  .flip-card {
    /* width: 300px; */
    height: 350px;
    perspective: 1000px;
    transition: 0.3s;
  }
  .flip-card-inner {
    position: relative;
    width: 100%;
    height: 100%;
    text-align: center;
    transition: transform 0.6s;
    transform-style: preserve-3d;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    background: #f8f9fa;
  }
  .flip-card:hover .flip-card-inner {
    transform: rotateY(180deg);
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
  }
  .flip-card-front, .flip-card-back {
    position: absolute;
    width: 100%;
    height: 100%;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
  }
  .flip-card-back {
    transform: rotateY(180deg);
  }

</style>
@endsection

@section('content')

	
   <!-- about breadcrumb -->
   <section class="w3l-about-breadcrumb text-left">
      <div class="breadcrumb-bg breadcrumb-bg-about py-sm-5 py-4">
         <div class="container py-2">
            <h2 class="title">Company</h2>
            <ul class="breadcrumbs-custom-path mt-2">
               <li><a href="{{url('/home')}}">Home</a></li>
               <li><span class="fa fa-arrow-right mx-2" aria-hidden="true"></span> Company </li>
               <li class="active"><span class="fa fa-arrow-right mx-2" aria-hidden="true"></span> {{!empty($company->company_name)? $company->company_name:''}} </li>
            </ul>
         </div>
      </div>
   </section>
   <!-- //about breadcrumb -->

   <!-- /company info-12-->
   <section class="w3l-cta4 py-1">
      <div class="container py-lg-2">
         <div class="ab-section">
            <h6 class="sub-title">Company</h6>
            <h3 class="hny-title">{{!empty($company->company_name)? $company->company_name:''}}</h3>
         </div>
         <div class="row mt-5">
            <div class="col-md-12">
               <p style="text-align: justify">{!!$company->body!!}</p>
            </div>
         </div>
      </div>
   </section>
   <!-- //company info-12-->


   <!-- /company category-12-->
   <section class="w3l-cta4 py-5">
      <div class="container py-lg-1">
         <div class="ab-section">
            <h6 class="sub-title">Product Category</h6>
            <!-- <h3 class="hny-title">{{!empty($company->company_name)? $company->company_name:''}}</h3> -->
         </div>
         <div class="row mt-5">
            @foreach($categories as $list)
              <div class="col-md-4 mb-4">

                <div class="flip-card" >
                  <div class="flip-card-inner" <?php if($list->image){ ?> style="background: url({{asset('uploads/'.$list->image)}}) no-repeat center; background-size: cover; z-index: 0;" <?php } else{ ?> style="background: {{$list->bg_color}}"  <?php } ?> >
                    <div class="flip-card-front" style="padding-top: 140px;">
                        <span style="font-size: 40px; <?php if($list->title_color){ ?> color: {{$list->title_color}} <?php } ?>">{{$list->category_name}}</span>
                    </div>
                    <div class="flip-card-back text-left" style="padding: 20px;">
                          <span>{{$list->category_name}}</span>
                          <hr>
                          <?php $flag=0; ?>
                          @foreach($sub_cat as $sc_list)
                            @if($sc_list->parent_id == $list->id)
                            <?php $flag=1; ?>
                              <li style="border-bottom: 1px solid #bfbfbf; padding: 4px;">
                                  <a href="{{url('product/category').'/'.$sc_list->id}}">{{$sc_list->category_name}}</a>
                              </li>
                            @endif
                          @endforeach
                          @if($flag==0)
                            <a href="{{url('product/category').'/'.$list->id}}">Learn more about {{$list->category_name}}</a>
                          @endif
                    </div>
                  </div>
                </div>

              </div>
            @endforeach
         </div>
      </div>
   </section>
   <!-- //company category-12-->


  <!-- Products -->
  <section class="w3l-clients" id="clients">
    <!-- /grids -->
    <div class="cusrtomer-layout py-1">
      <div class="container py-lg-4 py-md-3 pb-lg-0">
        <div class="heading text-left mx-auto">
          <h6 class="sub-title">Our Products</h6>
        </div>
        <!-- /grids -->
        <div class="testimonial-width mt-5">
          <div id="product-showcase" class="owl-two owl-carousel owl-theme">
              @foreach($products as $list)
                <div class="item" >
                  <div style="display: flex; ">
                      <div class="col-md-7" style="padding-top: 35px;">
                          <h4>{{$list->product_name}}</h4>
                          <p>{{$list->short_description}} . . .</p>
                          <a style="color: green;" href="{{url('product/details/'.$list->id)}}">Learn More >></a>
                      </div>
                      <div class="col-md-5" style="padding: 5px 0px">
                        <img src="{{asset('uploads/'.$list->image)}}"  style="height: 200px; width: auto" class="img-fluid" alt="client-img">
                      </div>
                  </div>
                </div>
              @endforeach
          </div>
        </div>
      </div>
      <!-- /grids -->
    </div>
    <!-- //grids -->
  </section>
  <!-- //Products -->

@endsection()



@section('extra_js')


<script>
		$(document).ready(function () {
			$("#product-showcase").owlCarousel({
				loop: true,
				margin: 8,
				nav: false,
				responsiveClass: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 1000,
				autoplayHoverPause: false,
				responsive: {
					0: {
						items: 1,
						nav: false
					},
					600: {
						items: 2,
						nav: false
					},
					1000: {
						items: 3,
						nav: false
					}
				}
			});
    });
  </script>

@endsection()
