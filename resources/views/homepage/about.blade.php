@extends('homepage.layout.master')

@section('extra_css')

@endsection

@section('content')
	
   <!-- about breadcrumb -->
   <section class="w3l-about-breadcrumb text-left">
      <div class="breadcrumb-bg breadcrumb-bg-about py-sm-5 py-4">
         <div class="container py-2">
            <h2 class="title">About Us</h2>
            <ul class="breadcrumbs-custom-path mt-2">
               <li><a href="{{url('/home')}}">Home</a></li>
               <li class="active"><span class="fa fa-arrow-right mx-2" aria-hidden="true"></span> About </li>
            </ul>
         </div>
      </div>
   </section>
   <!-- //about breadcrumb -->

   <!-- /about-6-->
   <section class="w3l-cta4 py-5">
      <div class="container py-lg-5">
         <div class="ab-section">
            <h6 class="sub-title">About Us</h6>
            <h3 class="hny-title">{{!empty($about->title)? $about->title:''}}</h3>
         </div>
         <div class="row mt-5">
            <div class="col-md-6">
               <p style="text-align: justify">{!! !empty($about->body)? $about->body:'' !!}</p>
            </div>
            <div class="col-md-6 mt-md-0 mt-4">
               <img src="{{!empty($about->image)? asset('uploads/'.$about->image):''}}" class="img-fluid" alt="">
            </div>
         </div>
      </div>
   </section>
   <!-- //about-6-->

  <!--/Features-->
  <section class="w3l-how-grids-3 py-1" id="how">
    <div class="container py-md-1">
      <div class="w3l-header mb-md-5 mb-4">
        <h6 class="sub-title">Why Choose Us</h6>
        <h3 class="hny-title">We Provide High-Quality <br> Agro Products</h3>
        <!-- <p class="">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p> -->
      </div>
      <div class="row bottom-ab-grids align-items-center">
        @foreach($features as $list)
          <div class="col-lg-3 bottom-ab-left" style="min-height: 500px;">
            <div class="grdhny-info">
              <img src="{{asset('uploads/'.$list->image)}}" class="img-curve img-fluid" alt="{{$list->meta}}" />
              <h4><a href="{{$list->url}}" class="hnys-title">{{$list->title}}</a></h4>
              <p class="mt-2" style="text-align: justify">{{str_limit($list->body, $limit = 240, $end = ' . . .')}}</p>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!--//Features-->

  <!-- achievements -->
  <section class="w3l-stats py-5" id="stats">
    <div class="gallery-inner container py-lg-0 py-3">
      <div class="row stats-con pb-lg-3">
        @foreach($achievements as $list)
          <div class="col-lg-3 col-6 stats_info counter_grid">
            <p class="counter">{{$list->total}}</p>
            <h4>{{$list->title}}</h4>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- //achievements -->




   <!-- team -->
   <!-- <section class="w3l-team" id="team">
      <div class="team-block py-5">
         <div class="container py-lg-5">
            <div class="title-content text-center mb-lg-3 mb-4">
               <h6 class="sub-title">Hard Working People</h6>
               <h3 class="hny-title">Our Team</h3>
            </div>
            <div class="row">
               <div class="col-lg-4 col-6 mt-lg-5 mt-4">
                  <div class="box16">
                     <a href="#url"><img src="assets/images/team1.jpg" alt="" class="img-fluid" /></a>
                     <div class="box-content">
                        <h3 class="title"><a href="#url">Alexander</a></h3>
                        <span class="post">Harvester</span>
                        <ul class="social">
                           <li>
                              <a href="#" class="facebook">
                              <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="twitter">
                              <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-6 mt-lg-5 mt-4">
                  <div class="box16">
                     <a href="#url"><img src="assets/images/team2.jpg" alt="" class="img-fluid" /></a>
                     <div class="box-content">
                        <h3 class="title"><a href="#url">Victoria</a></h3>
                        <span class="post">Harvester</span>
                        <ul class="social">
                           <li>
                              <a href="#" class="facebook">
                              <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="twitter">
                              <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-6 mt-lg-5 mt-4">
                  <div class="box16">
                     <a href="#url"><img src="assets/images/team3.jpg" alt="" class="img-fluid" /></a>
                     <div class="box-content">
                        <h3 class="title"><a href="#url">Smith roy</a></h3>
                        <span class="post">Harvester</a></span>
                        <ul class="social">
                           <li>
                              <a href="#" class="facebook">
                              <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="twitter">
                              <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-6 mt-lg-5 mt-4">
                  <div class="box16">
                     <a href="#url"><img src="assets/images/team4.jpg" alt="" class="img-fluid" /></a>
                     <div class="box-content">
                        <h3 class="title"><a href="#url">Johnson</a></h3>
                        <span class="post">Harvester</a></span>
                        <ul class="social">
                           <li>
                              <a href="#" class="facebook">
                              <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="twitter">
                              <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-6 mt-lg-5 mt-4">
                  <div class="box16">
                     <a href="#url"><img src="assets/images/team5.jpg" alt="" class="img-fluid" /></a>
                     <div class="box-content">
                        <h3 class="title"><a href="#url">Johnson</a></h3>
                        <span class="post">Harvester</a></span>
                        <ul class="social">
                           <li>
                              <a href="#" class="facebook">
                              <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="twitter">
                              <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-6 mt-lg-5 mt-4">
                  <div class="box16">
                     <a href="#url"><img src="assets/images/team6.jpg" alt="" class="img-fluid" /></a>
                     <div class="box-content">
                        <h3 class="title"><a href="#url">Johnson</a></h3>
                        <span class="post">Harvester</a></span>
                        <ul class="social">
                           <li>
                              <a href="#" class="facebook">
                              <span class="fa fa-facebook-f"></span>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="twitter">
                              <span class="fa fa-twitter"></span>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section> -->
   <!-- //team -->


@endsection()



@section('extra_js')

@endsection()
