@extends('homepage.layout.master')

@section('extra_css')

@endsection

@section('content')
	
   <!-- about breadcrumb -->
   <section class="w3l-about-breadcrumb text-left">
      <div class="breadcrumb-bg breadcrumb-bg-about py-sm-5 py-4">
         <div class="container py-2">
            <h2 class="title">Company</h2>
            <ul class="breadcrumbs-custom-path mt-2">
               <li><a href="{{url('/home')}}">Home</a></li>
               <li><span class="fa fa-arrow-right mx-2" aria-hidden="true"></span> Product View</li>
            </ul>
         </div>
      </div>
   </section>
   <!-- //about breadcrumb -->

   <!-- /Product-6-->
   <section class="w3l-cta4 py-5">
      <div class="container py-lg-5">
         <div class="ab-section">
            <h6 class="sub-title">Product Specification</h6>
         </div>
         <div class="row mt-5">
            <div class="col-md-8">
                  {!!$product->description!!}
            </div>
            <div class="col-md-4 text-center">
               <img src="{{asset('uploads/'.$product->image)}}" class="img-fluid" alt="client-img">
            </div>
         </div>
      </div>
   </section>
   <!-- //Product-6-->
@endsection()



@section('extra_js')

@endsection()
