@extends('dashboard.layout.master')

@section('extra_css')

@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/content')}}">Uploades Files</a></li>
			<li class="active">Achievements</li>
		</ol>

		<div class="row">
			<div class="col-lg-12 col-md-12 mb-4 mb-lg-0">
				@foreach($files as $path)
					<img src="{{url('/uploads'.'/'.$path->getFilename())}}" style="width: 220px; height: auto" class="image w-100 shadow-1-strong rounded mb-4" alt="" />
				@endforeach
			</div>
		</div>
		<!-- Page Content End -->

   </div>
</div>
@endsection()



@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
	$('.image').click(function(){
	// Create an auxiliary hidden input
	var aux = document.createElement("input");

	// Get the text from the element passed into the input
	aux.setAttribute("value", $(this).attr('src'));

	// Append the aux input to the body
	document.body.appendChild(aux);

	// Highlight the content
	aux.select();

	// Execute the copy command
	document.execCommand("copy");

	// Remove the input from the body
	document.body.removeChild(aux);
		alert('Media url copyed');
	});
</script>

@endsection()
