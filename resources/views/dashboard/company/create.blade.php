@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/companies')}}">Companies</a></li>
			<li class="active">Add New</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Add New Company</h4>
				</div>
				
				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')

				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/companies?page=save')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Company Name</label>
							<div class="col-sm-6">
								<input type="text" name="company_name" value="{{!empty($item->company_name) ? $item->company_name : old('company_name')}}" class="form-control"  placeholder="Company Name" minlength="1" maxlength="50" required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Company name here. Max 50 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Image</label>
							<div class="col-sm-6">
								<input type="file" name="image" class="form-control" accept="image/png, image/gif, image/jpeg"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 5MB. &nbsp; JPEG/PNG only</p>
							</div>
						</div>

						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Body</label>
							<div class="col-sm-6">
								<textarea name="body" id="txtarea" cols="50" rows="10" class="form-control">{{!empty($item->body) ? $item->body : old('body')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Body here</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Short URL</label>
							<div class="col-sm-6">
								<input type="text" name="url" value="{{!empty($item->url) ? $item->url : old('url')}}" class="form-control"  placeholder="URL" minlength="4" maxlength="50"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Company Short URL here. Max 50 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Sequence</label>
							<div class="col-sm-6">
								<input type="number" name="sequence"  value="{{!empty($item->sequence) ? $item->sequence : old('sequence')}}" class="form-control"  placeholder="Sequence" minlength="1" maxlength="2"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Sequence here. Default : 1</p>
							</div>
						</div>
						
						<div class="form-group">
							<label for="radio" class="col-sm-2 control-label">Status</label>
							<div class="col-sm-6">
								<div class="radio-inline"><label><input name="status" type="radio" value="Active" checked=""> Active</label></div>
								<div class="radio-inline"><label><input name="status" type="radio" value="Inactive" > Inactive</label></div>
							</div>
						</div>
						
						<center>
							<button type="submit" class="btn btn-success" style="margin-right: 20px">Submit</button>
							<a href="{{url('/dashboard/companies')}}" class="btn btn-warning">Cancel</a>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
	$('document').ready(function(){
		CKEDITOR.replace( 'body' );
	});
</script>

@endsection()
