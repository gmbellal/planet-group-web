@extends('dashboard.layout.master')

@section('extra_css')

@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li class="active">Companies</li>
		</ol>

		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<div class="row">
					<div class="col-md-6"><h4>Company List</h4></div>
					<div class="col-md-6"><a href="{{url('/dashboard/companies?page=create')}}" class="btn btn-primary btn-flat btn-pri pull-right">  <i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
				</div>
				
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Company Name</th>
							<th class="text-center">Image</th>
							<th class="text-center">Sequence</th>
							<th class="text-center">URL</th>
							<th class="text-center">Active</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1  @endphp
						@foreach($companies as $list)
						<tr>
							<th scope="row">{{$i++}}</th>
							<td>{{$list->company_name}}</td>
							<td class="text-center">
								<img src="{{asset('uploads/'.$list->image)}}" height="50px">
							</td>
							<td class="text-center">{{$list->sequence}}</td>
							<td class="text-center"><a target="_blank" href="{{$list->url}}"><i class="fa fa-link" aria-hidden="true"></i></a></td>
							<td class="text-center">
								@if($list->status == 'Active')
									<label class="switch"><input type="checkbox" onclick="location.href='{{url('/dashboard/companies?page=status&ref=Active&id='.$list->id)}}'" checked><span class="slider round"></span></label>
								@else
									<label class="switch"><input type="checkbox" onclick="location.href='{{url('/dashboard/companies?page=status&ref=Inactive&id='.$list->id)}}'" ><span class="slider round"></span></label>
								@endif
							</td>
							<td class="text-center">
								<a href="{{url('/dashboard/companies?page=edit&id=').$list->id}}" class="btn btn-success btn-sm">Edit</a>
								<a href="{{url('/dashboard/companies?page=delete&id=').$list->id}}" class="btn btn-danger btn-sm">Delete</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- Page Content End -->
   </div>
</div>
@endsection()



@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
