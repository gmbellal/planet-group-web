@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/categories')}}">Categories</a></li>
			<li class="active">Add New</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Add New Category</h4>
				</div>
				
				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')

				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/categories?page=save')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Company</label>
							<div class="col-sm-6">
								<select name="company_id" class="form-control" required>
									<option value="" >Select Company</option>
									@foreach($companies as $list)	
										<option value="{{$list->id}}" {{ isset($item)? ($item->company_id == $list->id? 'selected': '') : ($list->id == old('company_id') ?'selected': '') }} >{{$list->company_name}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Select company from list</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Parent Category</label>
							<div class="col-sm-6">
								<select name="parent_id" class="form-control">
									<option value="" >Root</option>	
									@foreach ($categories as $key => $cat)	
										<optgroup label="{{$cat[0]->company_name}}">
											@foreach ($cat as $list)	
												<option value="{{$list->id}}" {{isset($item)? ($item->parent_id == $list->id? 'selected': '') : ($list->id == old('parent_id') ?'selected': '') }} >{{$list->category_name}}</option>
											@endforeach
										</optgroup>
									@endforeach
								</select>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Select brand/partner from list</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Category Name</label>
							<div class="col-sm-6">
								<input type="text" name="category_name" value="{{!empty($item->category_name) ? $item->category_name : old('category_name')}}" class="form-control"  placeholder="Category Name" minlength="1" maxlength="50" required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Category name here. Max. 50 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Image</label>
							<div class="col-sm-6">
								<input type="file" name="image" class="form-control" accept="image/png, image/gif, image/jpeg" >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 5MB. &nbsp; JPEG/PNG only</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Sequence</label>
							<div class="col-sm-6">
								<input type="number" name="sequence"  value="{{!empty($item->sequence) ? $item->sequence : old('sequence')}}" class="form-control"  placeholder="Sequence" minlength="1" maxlength="2"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Sequence here. Default : 1</p>
							</div>
						</div>
						
						<center>
							<button type="submit" class="btn btn-success" style="margin-right: 20px">Submit</button>
							<a href="{{url('/dashboard/categories')}}" class="btn btn-warning">Cancel</a>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
