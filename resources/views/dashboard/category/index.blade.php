@extends('dashboard.layout.master')

@section('extra_css')

@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li class="active">Categories</li>
		</ol>
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<div class="row">
					<div class="col-md-6"><h4>Category List</h4></div>
					<div class="col-md-6"><a href="{{url('/dashboard/categories?page=create')}}" class="btn btn-primary btn-flat btn-pri pull-right">  <i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div>
				</div>
				
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Company Name</th>
							<th>Category</th>
							<th class="text-center">Image</th>
							<th class="text-center">Sequence</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1  @endphp
						@foreach($categories as $list)
						<tr>
							<th scope="row">{{$i++}}</th>
							<td>{{$list->company_name}}</td>
							@if($list->parent)
								<td><i class="fa fa-home" aria-hidden="true"></i> {{$list->parent}} <i class="fa fa-caret-right" aria-hidden="true"></i> {{$list->category_name}}</td>
							@else
								<td><i class="fa fa-home" aria-hidden="true"></i> {{$list->category_name}}</td>
							@endif
							<td class="text-center">
								<img src="{{asset('uploads/'.$list->image)}}" height="50px">
							</td>
							<td class="text-center">{{$list->sequence}}</td>
							<td class="text-center">
								<a href="{{url('/dashboard/categories?page=edit&id=').$list->id}}" class="btn btn-success btn-sm">Edit</a>
								<a href="{{url('/dashboard/categories?page=delete&id=').$list->id}}" class="btn btn-danger btn-sm">Delete</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- Page Content End -->
   </div>
</div>
@endsection()



@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
