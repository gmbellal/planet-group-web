@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/products')}}">Products</a></li>
			<li class="active">Add New</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Add New Product</h4>
				</div>
				





				



				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')

				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/products?page=save')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Category</label>
							<div class="col-sm-6">
								<select name="category_id" class="form-control" required>
									<option value="" >Select Category</option>	
									@foreach ($categories as $key => $cat)	
										<optgroup label="{{$key}}">
											@foreach ($categories[$key] as $key2 => $list)
												@if(isset($list[0]->category_name))
													<optgroup label="{{$list[0]->parent_name}}">
														@foreach ($list as $child_cat)
															<option value="{{$child_cat->id}}" {{isset($item)? ($item->category_id == $child_cat->id? 'selected': '') : ($child_cat->id == old('category_id') ? 'selected' : '') }}>{{$child_cat->category_name}}</option>
														@endforeach
													</optgroup>
												@else
													<option value="{{$list->id}}" {{isset($item)? ($item->category_id == $list->id? 'selected': '') : ($list->id == old('category_id') ? 'selected' : '') }} >{{$list->category_name}}</option>
												@endif
											@endforeach
										</optgroup>
									@endforeach
								</select>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Select category from list</p>
							</div>
						</div>


						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Product Name</label>
							<div class="col-sm-6">
								<input type="text" name="product_name" value="{{!empty($item->product_name) ? $item->product_name : old('product_name')}}" class="form-control"  placeholder="Product Name" minlength="1" maxlength="50" required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Product name here. Max. 50 characters</p>
							</div>
						</div>


						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Short Description</label>
							<div class="col-sm-6">
								<input type="text" name="short_description" value="{{!empty($item->short_description) ? $item->short_description : old('short_description')}}" class="form-control"  placeholder="Short Description" minlength="1" maxlength="60" required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Product name here. Max. 60 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Product Description</label>
							<div class="col-sm-6">
								<textarea name="description" id="txtarea" cols="50" rows="5" class="form-control" required >{{!empty($item->description) ? $item->description : old('description')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Product description here<br>Max. 5000 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Image</label>
							<div class="col-sm-6">
								<input type="file" name="image" class="form-control" accept="image/png, image/gif, image/jpeg"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 5MB. &nbsp; JPEG/PNG only</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Sequence</label>
							<div class="col-sm-6">
								<input type="number" name="sequence"  value="{{!empty($item->sequence) ? $item->sequence : old('sequence')}}" class="form-control"  placeholder="Sequence" minlength="1" maxlength="2"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Sequence here. Default : 1</p>
							</div>
						</div>
						
						<div class="form-group">
							<label for="radio" class="col-sm-2 control-label">Status</label>
							<div class="col-sm-6">
								<div class="radio-inline"><label><input name="status" type="radio" value="Active" checked=""> Active</label></div>
								<div class="radio-inline"><label><input name="status" type="radio" value="Inactive" > Inactive</label></div>
							</div>
						</div>
						
						<center>
							<button type="submit" class="btn btn-success" style="margin-right: 20px">Submit</button>
							<a href="{{url('/dashboard/products')}}" class="btn btn-warning">Cancel</a>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()





@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
	$('document').ready(function(){
		CKEDITOR.replace( 'description' );
	});
	
</script>

@endsection()
