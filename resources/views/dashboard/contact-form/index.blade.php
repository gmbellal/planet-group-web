@extends('dashboard.layout.master')

@section('extra_css')

@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li class="active">Contact Form</li>
		</ol>
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<div class="row">
					<div class="col-md-6"><h4>Contact Form List</h4></div>
				</div>
				
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Date</th>
							<th>Name</th>
							<th>Phone</th>
							<th>Email</th>
							<th>Subject</th>
							<th>Message</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1  @endphp
						@foreach($contact_form as $list)
						<tr>
							<th>{{$i++}}</th>
							<td style="width: 110px;">{{date('d M, Y h:i:a', strtotime($list->created_at))}}</td>
							<td>{{$list->name}}</td>
							<td>{{$list->phone_number}}</td>
							<td>{{$list->email}}</td>
							<td>{{$list->subject}}</td>
							<td>{{$list->message}}</td>
							<!-- <td class="text-center">
								<a href="{{url('/dashboard/contact-form?page=delete&id=').$list->id}}" class="btn btn-danger btn-sm">Delete</a>
							</td> -->
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- Page Content End -->
   </div>
</div>
@endsection()



@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
