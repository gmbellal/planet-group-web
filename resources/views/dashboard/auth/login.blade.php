<!DOCTYPE HTML>
<html>
<head>
    <title>Login Page :: Planet Group</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Login Page of Planet Group" />
    <link href="{{asset('assets/dashboard/css/font-awesome.css')}}" rel="stylesheet"> 
    <link href="{{asset('assets/dashboard/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('assets/dashboard/css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <script src="{{asset('assets/dashboard/js/jquery-1.11.1.min.js')}}"></script>
    <style>
        .login-footer {
            background: #fff;
            padding: 1em;
            width: 100%;
            text-align: center;
            border-bottom: 1px solid #e0e0e0;
            box-shadow: 0px -1px 4px rgb(0 0 0 / 21%);
            -webkit-box-shadow: 0px -1px 4px rgb(0 0 0 / 21%);
            -moz-box-shadow: 0px -1px 4px rgba(0, 0, 0, 0.21);
            -ms-box-shadow: 0px -1px 4px rgba(0, 0, 0, 0.21);
            -o-box-shadow: 0px -1px 4px rgba(0, 0, 0, 0.21);
            
        }
    </style>
</head> 
<body class="cbp-spmenu-push">
    <div class="main-content"> 
        <!-- main content start-->
        <div class="main-page login-page ">
            
            <h2 class="title1">Login</h2>
            <div class="widget-shadow">
                <div class="login-body">
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <input type="text" class="user" name="username" placeholder="Enter Your Username" required="">
                        <input type="password" name="password" class="lock" placeholder="Password" required="">
                        <div class="forgot-grid">
                            <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Remember me</label>
                            <div class="forgot">
                                <a href="#">forgot password?</a>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <input type="submit" name="Sign In" value="Sign In">
                        <div class="registration">
                            Don't have an account ? Contact with system admin.
                        </div>
                    </form>
                </div>
            </div>
            <!--footer-->
            <div class="login-footer">
                <p>&copy; {{date('Y')}} Planet Group. All Rights Reserved | Developed by <a href="https://planet.com.bd" target="_blank">Planet Group</a></p>
            </div>
            <!--//footer-->
        </div>
    </div>
</body>
</html>