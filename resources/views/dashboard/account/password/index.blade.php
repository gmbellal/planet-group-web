@extends('dashboard.layout.master')

@section('extra_css')

@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/account')}}">Account</a></li>
			<li class="active">Change Password</li>
		</ol>

		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<div class="row">
					<div class="col-md-6"><h4>Change Account Password</h4></div>
					<div class="col-md-6"><a href="{{url('/dashboard/account')}}" class="btn btn-primary btn-flat btn-pri pull-right">  <i class="fa fa-reply" aria-hidden="true"></i> Back</a></div>
				</div>

				@include('dashboard.layout.common.validation_error')
				
				<table class="table table-bordered">
					<form action="{{url('/dashboard/account/change-password?page=update')}}"  method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<tbody>
							<tr>
								<td class="col-md-3">Username: </td>
								<td><input value="{{$profile->username}}" class="form-control" readonly/></td>
							</tr>
							<tr>
								<td class="col-md-3">Current Password: </td>
								<td>
									<input type="password" name="current_password" placeholder="Current Password" class="form-control" >
								</td>
							</tr>
							<tr>
								<td class="col-md-3">New Password: </td>
								<td>
									<input type="password" name="new_password" placeholder="New Password" class="form-control">
								</td>
							</tr>
							<tr>
								<td class="col-md-3">Confirm New Password: </td>
								<td>
									<input type="password" name="confirm_new_password" placeholder="Confirm New Password" class="form-control">
								</td>
							</tr>
							<tr>
								<td colspan="2" class="text-center"><button type="submit" class="btn btn-success">Update Profile</button></td>
							</tr>
						</tbody>
					</form>
				</table>
			</div>
		</div>
		<!-- Page Content End -->
   </div>
</div>
@endsection()



@section('extra_js')
<!-- Extra page js-->
<script>
	
</script>
@endsection()
