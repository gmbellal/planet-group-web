@extends('dashboard.layout.master')

@section('extra_css')

@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/account')}}">Account</a></li>
			<li class="active">Index</li>
		</ol>

		

		<div class="col_3" style="margin-top: 50px">
        	
			<div class="col-md-3 widget widget1">
				<a href="{{url('/dashboard/account/profile')}}">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-user icon-rounded"></i>
						<div class="stats">
						<h5><strong>View</strong></h5>
						<span>My Profile</span>
						</div>
					</div>
				</a>
        	</div>
			
        	<div class="col-md-3 widget widget1">
				<a href="{{url('/dashboard/account/change-password')}}">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-key user1 icon-rounded"></i>
						<div class="stats">
						<h5><strong>Change</strong></h5>
						<span>password</span>
						</div>
					</div>
				</a>
        	</div>

        	<!-- <div class="col-md-3 widget widget1">
				<a href="{{url('/dashboard/account/profile/change-password')}}">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-key dollar2 icon-rounded"></i>
						<div class="stats">
						<h5><strong>Change</strong></h5>
						<span>password</span>
						</div>
					</div>
				</a>
        	</div>
			
        	<div class="col-md-3 widget widget1">
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-pie-chart dollar1 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong>$450</strong></h5>
                      <span>Expenditure</span>
                    </div>
                </div>
        	 </div>
        	<div class="col-md-3 widget">
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong>1450</strong></h5>
                      <span>Total Users</span>
                    </div>
                </div>
        	</div> -->
        	<div class="clearfix"> </div>
		</div>

		<!-- Page Content End -->
   </div>
</div>
@endsection()



@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
