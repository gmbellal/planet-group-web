@extends('dashboard.layout.master')

@section('extra_css')

@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/account')}}">Account</a></li>
			<li class="active">Profile</li>
		</ol>

		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<div class="row">
					<div class="col-md-6"><h4>My Profile</h4></div>
					<div class="col-md-6"><a href="{{url('/dashboard/account')}}" class="btn btn-primary btn-flat btn-pri pull-right">  <i class="fa fa-reply" aria-hidden="true"></i> Back</a></div>
				</div>

				@include('dashboard.layout.common.validation_error')
				
				<table class="table table-bordered">
					<form action="{{url('/dashboard/account/profile?page=update')}}"  method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<input name="old_image" value="{{$profile->image}}" hidden>
						<tbody>
							<tr>
								<td>Username: </td>
								<td><input value="{{$profile->username}}" class="form-control" readonly/></td>
								<td rowspan="5" class="text-center avatar">
									@if($profile->image)
										<img src="{{asset('uploads/'.$profile->image)}}" style="width: 150px; height: auto" alt="Avatar">
									@else
										No Photo Selected
									@endif
								</td>
							</tr>
							<tr>
								<td>Full Name: </td>
								<td>
									<input type="text" name="fullname" value="{{$profile->fullname}}" placeholder="Full Name" class="form-control"  required>
								</td>
							</tr>
							<tr>
								<td>Photo: </td>
								<td><input type="file" onchange="readURL($(this))" name="image" class="form-control" accept="image/png, image/gif, image/jpeg" id="imgInp"></td>
							</tr>
							<tr>
								<td>Account Status: </td>
								<td>{{$profile->active_status}}</td>
							</tr>
							<tr>
								<td colspan="2" class="text-center"><button type="submit" class="btn btn-success">Update Profile</button></td>
							</tr>
						</tbody>
					</form>
				</table>
			</div>
		</div>
		<!-- Page Content End -->
   </div>
</div>
@endsection()



@section('extra_js')
<!-- Extra page js-->
<script>
	imgInp.onchange = evt => {
		const [file] = imgInp.files
		if (file) {
			$('td.avatar').html("<img id='avatar' src='#' alt='avatar' style='width: 150px; height: auto' />")
			avatar.src = URL.createObjectURL(file)
		}
	}

</script>
@endsection()
