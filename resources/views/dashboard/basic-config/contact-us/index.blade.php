@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/basic-config')}}">Basic Config</a></li>
			<li class="active">Contact Us</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Setup Contact Us Information</h4>
				</div>
				
				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')

				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/basic-config/contact-us?page=update')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-6">
								<input type="email" name="email" value="{{!empty($contact->email) ? $contact->email : old('email')}}" class="form-control"  placeholder="Email" minlength="5" maxlength="50"  >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Email here. Max. 50 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Phone</label>
							<div class="col-sm-6">
								<input type="tel" name="phone" value="{{!empty($contact->phone) ? $contact->phone : old('phone')}}" class="form-control"  placeholder="Phone" minlength="10" maxlength="15"  >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Phone here. Max. 15 characters</p>
							</div>
						</div>
						
						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Address</label>
							<div class="col-sm-6">
								<textarea name="address" id="txtarea" cols="50" rows="2" class="form-control">{{!empty($contact->address) ? $contact->address : old('address')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 150 characters</p>
							</div>
						</div>
						
						<center style="margin-top: 50px !important;">
							<button type="submit" class="btn btn-success">Update</button>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
