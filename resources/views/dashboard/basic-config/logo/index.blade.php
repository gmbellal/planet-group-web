@extends('dashboard.layout.master')



@section('extra_css')

@endsection


@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- Page Content Start -->

		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/basic-config')}}">Basic Config</a></li>
			<li class="active">Brand Logo</li>
		</ol>



		<div class="tables">
			
			<div class="table-responsive bs-example widget-shadow">
				<div class="row">
					<div class="col-md-6"><h4>Setup Brand Logo</h4></div>
					<!-- <div class="col-md-6"><a href="{{url('/dashboard/basic-config/brand-logo?page=create')}}" class="btn btn-primary btn-flat btn-pri pull-right">  <i class="fa fa-plus" aria-hidden="true"></i> Add New</a></div> -->
				</div>
				
			

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th class="text-center">Url</th>
							<th>Image</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						@php $i = 1  @endphp
						@foreach($sliders as $list)
						<tr>
							<th scope="row">{{$i++}}</th>
							<td>{{$list->title}}</td>
							<td class="text-center"><a target="_blank" href="{{$list->url}}"><i class="fa fa-link" aria-hidden="true"></i></a></td>
							<td>{{$list->title}}</td>
							<td class="text-center">

							
							<button type="button" class="btn btn-primary btn-sm">Edit</button>
							<button type="button" class="btn btn-danger btn-sm">Delete</button>
							<button type="button" class="btn btn-success btn-sm">View</button>

								
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<!-- Page Content End -->
   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
