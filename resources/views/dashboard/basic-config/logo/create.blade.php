@extends('dashboard.layout.master')


@section('extra_css')
	
@endsection


@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/basic-config')}}">Basic Config</a></li>
			<li><a href="{{url('/dashboard/basic-config/home-slider')}}">Home Slider</a></li>
			<li class="active">Add New</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Add New Slider</h4>
				</div>
				<div class="form-body">
				
					<form class="form-horizontal" action="{{url('/dashboard/basic-config/home-slider?page=save')}}"  method="post" enctype="multipart/form-data" >
						
						{{ csrf_field() }}
						<div class="form-group">
							<label for="focusedinput" class="col-sm-2 control-label">Focused Input</label>
							<div class="col-sm-8">
								<input type="text" class="form-control1" id="focusedinput" placeholder="Default Input">
							</div>
							<div class="col-sm-2">
								<p class="help-block">Your help text!</p>
							</div>
						</div>

						<div class="form-group">
							<label for="disabledinput" class="col-sm-2 control-label">Disabled Input</label>
							<div class="col-sm-8">
								<input disabled="" type="text" class="form-control1" id="disabledinput" placeholder="Disabled Input">
							</div>
						</div>

						<div class="form-group">
							<label for="inputPassword" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control1" id="inputPassword" placeholder="Password">
							</div>
						</div>
						
						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Meta Tag</label>
							<div class="col-sm-8"><textarea name="txtarea1" id="txtarea1" cols="50" rows="4" class="form-control1"></textarea></div>
						</div>
						
						<div class="form-group">
							<label for="radio" class="col-sm-2 control-label">Status</label>
							<div class="col-sm-8">
								<div class="radio-inline"><label><input type="radio"> Active</label></div>
								<div class="radio-inline"><label><input type="radio" checked=""> Inactive</label></div>
							</div>
						</div>

						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Medium Input</label>
							<div class="col-sm-8">
								<input type="text" class="form-control1" id="mediuminput" placeholder="Medium Input">
							</div>
						</div>

						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Disabled Input</label>
							<div class="col-sm-8">
								<input type="file" type="text" class="form-control1"  >
							</div>
							<div class="col-sm-2">
								<p class="help-block">Max. 32MB</p>
							</div>
						</div>
						
						<center>
							<button type="submit" class="btn btn-success" style="margin-right: 20px">Submit</button>
							<button type="submit" class="btn btn-warning">Cancel</button>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
