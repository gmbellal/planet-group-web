@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/basic-config')}}">Basic Config</a></li>
			<li class="active">Services</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Setup Services Information</h4>
				</div>
				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')
				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/basic-config/services?page=update')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Title</label>
							<div class="col-sm-6">
								<input type="text" name="title" value="{{!empty($services->title) ? $services->title : old('title')}}" class="form-control"  placeholder="Title" minlength="3" maxlength="50"  >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Title here. Max. 50 characters</p>
							</div>
						</div>
						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Body</label>
							<div class="col-sm-6">
								<textarea name="body" id="txtarea" cols="50" rows="7" class="form-control">{{!empty($services->body) ? $services->body : old('body')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">About us body here</p>
							</div>
						</div>
						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Services</label>
							<div class="col-sm-6">
								<textarea name="services" id="txtarea" cols="50" rows="7" class="form-control">{{!empty($services->services) ? $services->services : old('services')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Services here. Max. 500 characters<br>Use new line between different services</p>
							</div>
						</div>
						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Image</label>
							<div class="col-sm-6">
								<input type="file" name="image" class="form-control" accept="image/png, image/gif, image/jpeg" >
								<input type="text" name="old_image" value="{{!empty($services->image)? $services->image : ''}}" hidden>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 5MB. &nbsp; JPEG/PNG only</p>
							</div>
						</div>

						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Meta Tag</label>
							<div class="col-sm-6">
								<textarea name="meta" id="txtarea" cols="50" rows="2" class="form-control">{{!empty($services->meta) ? $services->meta : old('meta')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 100 characters</p>
							</div>
						</div>
						
						<center style="margin-top: 50px !important;">
							<button type="submit" class="btn btn-success">Update</button>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
