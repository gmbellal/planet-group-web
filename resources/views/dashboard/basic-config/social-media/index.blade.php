@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/basic-config')}}">Basic Config</a></li>
			<li class="active">Social Media</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Setup Social Media Information</h4>
				</div>
				
				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')

				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/basic-config/social-media?page=update')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Facebook</label>
							<div class="col-sm-6">
								<input type="text" name="facebook" value="{{!empty($social_media->facebook) ? $social_media->facebook : old('facebook')}}" class="form-control"  placeholder="Facebook" minlength="1" maxlength="200"  >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Facebook url here. Max. 200 characters</p>
							</div>
						</div>
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Twitter</label>
							<div class="col-sm-6">
								<input type="text" name="twitter" value="{{!empty($social_media->twitter) ? $social_media->twitter : old('twitter')}}" class="form-control"  placeholder="Twitter" minlength="1" maxlength="200"  >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Twitter url here. Max. 200 characters</p>
							</div>
						</div>
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Instagram</label>
							<div class="col-sm-6">
								<input type="text" name="instagram" value="{{!empty($social_media->instagram) ? $social_media->instagram : old('instagram')}}" class="form-control"  placeholder="Instagram" minlength="1" maxlength="200"  >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Instagram url here. Max. 200 characters</p>
							</div>
						</div>
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Linkedin</label>
							<div class="col-sm-6">
								<input type="text" name="linkedin" value="{{!empty($social_media->linkedin) ? $social_media->linkedin : old('linkedin')}}" class="form-control"  placeholder="Linkedin" minlength="1" maxlength="200"  >
							</div>
							<div class="col-sm-4">
								<p class="help-block">Linkedin url here. Max. 200 characters</p>
							</div>
						</div>
						
						
						
						<center style="margin-top: 50px !important;">
							<button type="submit" class="btn btn-success">Update</button>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
