<!doctype html>
<html lang="en">

	<head>
		<title>Dashboard : Planet Group</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Web Admin Panel" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- Bootstrap Core CSS -->
		<link href="{{asset('assets/dashboard/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
		<!-- Custom CSS -->
		<link href="{{asset('assets/dashboard/css/style.css')}}" rel='stylesheet' type='text/css' />
		<!-- font-awesome icons CSS -->
		<link href="{{asset('assets/dashboard/css/font-awesome.css')}}" rel="stylesheet"> 
		<!-- //font-awesome icons CSS-->
		<!-- side nav css file -->
		<link href="{{asset('assets/dashboard/css/SidebarNav.min.css')}}" media='all' rel='stylesheet' type='text/css'/>
		<!-- //side nav css file -->
		<!-- js-->
		<script src="{{asset('assets/dashboard/js/jquery-1.11.1.min.js')}}"></script>
		<script src="{{asset('assets/dashboard/js/modernizr.custom.js')}}"></script>
		<!--webfonts-->
		<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
		<!--//webfonts--> 
		<!-- chart -->
		<script src="{{asset('assets/dashboard/js/Chart.js')}}"></script>
		<!-- //chart -->
		<!-- Metis Menu -->
		<script src="{{asset('assets/dashboard/js/metisMenu.min.js')}}"></script>
		<script src="{{asset('assets/dashboard/js/custom.js')}}"></script>
		<link href="{{asset('assets/dashboard/css/custom.css')}}" rel="stylesheet">
		<!--//Metis Menu -->
		<style>
			#chartdiv {
				width: 100%;
				height: 295px;
			}
		</style>
		<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
		<script src="{{asset('assets/dashboard/js/pie-chart.js')}}" type="text/javascript"></script>
		<script type="text/javascript">

			$(document).ready(function () {
				$('#demo-pie-1').pieChart({
					barColor: '#2dde98',
					trackColor: '#eee',
					lineCap: 'round',
					lineWidth: 8,
					onStep: function (from, to, percent) {
						$(this.element).find('.pie-value').text(Math.round(percent) + '%');
					}
				});

				$('#demo-pie-2').pieChart({
					barColor: '#8e43e7',
					trackColor: '#eee',
					lineCap: 'butt',
					lineWidth: 8,
					onStep: function (from, to, percent) {
						$(this.element).find('.pie-value').text(Math.round(percent) + '%');
					}
				});

				$('#demo-pie-3').pieChart({
					barColor: '#ffc168',
					trackColor: '#eee',
					lineCap: 'square',
					lineWidth: 8,
					onStep: function (from, to, percent) {
						$(this.element).find('.pie-value').text(Math.round(percent) + '%');
					}
				});

			
			});

		</script>
		<!-- //pie-chart --><!-- index page sales reviews visitors pie chart -->

		<!-- requried-jsfiles-for owl -->
		<link href="{{asset('assets/dashboard/css/owl.carousel.css')}}" rel="stylesheet">
		<script src="{{asset('assets/dashboard/js/owl.carousel.js')}}"></script>
		<script>
			$(document).ready(function() {
				$("#owl-demo").owlCarousel({
					items : 3,
					lazyLoad : true,
					autoPlay : true,
					pagination : true,
					nav:true,
				});
			});
		</script>
		<!-- //requried-jsfiles-for owl -->
		<script src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
	</head> 

	<body class="cbp-spmenu-push">
		<div class="main-content">
			@include('dashboard.layout.common.menu')
			@include('dashboard.layout.common.header')
			@yield('content')
			@include('dashboard.layout.common.footer')
		</div>



		<!-- Script -->
		<script src="{{asset('assets/dashboard/js/Chart.bundle.js')}}"></script>
		<script src="{{asset('assets/dashboard/js/utils.js')}}"></script>
		<!--scrolling js-->
		<script src="{{asset('assets/dashboard/js/jquery.nicescroll.js')}}"></script>
		<script src="{{asset('assets/dashboard/js/scripts.js')}}"></script>
		<!--//scrolling js-->
		<script src="{{asset('assets/dashboard/js/classie.js')}}"></script>
		<!-- side nav js -->
		<script src="{{asset('assets/dashboard/js/SidebarNav.min.js')}}" type='text/javascript'></script>
		<!-- for index page weekly sales java script -->
		<script src="{{asset('assets/dashboard/js/SimpleChart.js')}}"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="{{asset('assets/dashboard/js/bootstrap.js')}}"> </script>
		<!-- //Bootstrap Core JavaScript -->

		
		@yield('extra_js')
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}

			$(document).ready(function() {
				$('.sidebar-menu').SidebarNav();
			});
		</script>


	</body>
</html>