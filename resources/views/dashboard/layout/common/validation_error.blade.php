@if($errors->any())
  <div class="alert alert-danger" style="margin: 10px;" role="alert">
    @foreach ($errors->all() as $error)
      <p>{{ $error }}</p>
    @endforeach
  </div>
@endif