<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
			<!--left-fixed -navigation-->
			<aside class="sidebar-left">
				<nav class="navbar navbar-inverse">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<h1><a class="navbar-brand" href="{{url('home')}}" target="_blank"><span class="fa fa-area-chart"></span> Admin Panel<span class="dashboard_text">Welcome</span></a></h1>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="sidebar-menu">
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview">
							<a href="{{url('dashboard')}}">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
							</a>
						</li>
						
						
						<!-- <li class="treeview">
							<a href="#">
							<i class="fa fa-laptop"></i>
							<span>Components</span>
							<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
							<li><a href="grids.html"><i class="fa fa-angle-right"></i> Grids</a></li>
							<li><a href="media.html"><i class="fa fa-angle-right"></i> Media Css</a></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="charts.html">
							<i class="fa fa-pie-chart"></i>
							<span>Charts</span>
							<span class="label label-primary pull-right">new</span>
							</a>
						</li>
						<li class="treeview"> -->


						<li class="treeview {{str_contains(Request::path(), 'basic-config') ? 'active' : '' }}">
							<a href="#">
							<i class="fa fa-wrench"></i>
							<span>Basic Config</span>
							<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<!-- <li class="{{str_contains(Request::path(), '/brand-logo') ? 'active' : '' }}"><a href="{{url('/dashboard/basic-config/brand-logo')}}"><i class="fa fa-picture-o"></i> Brand Logo</a></li> -->
								<li class="{{str_contains(Request::path(), '/home-slider') ? 'active' : '' }}"><a href="{{url('/dashboard/basic-config/home-slider')}}"><i class="fa fa-sliders"></i> Home Slider</a></li>
								<li class="{{str_contains(Request::path(), '/about-us') ? 'active' : '' }}"><a href="{{url('/dashboard/basic-config/about-us')}}"><i class="fa fa-info-circle"></i> About Us</a></li>
								<li class="{{str_contains(Request::path(), '/services') ? 'active' : '' }}"><a href="{{url('/dashboard/basic-config/services')}}"><i class="fa fa-briefcase"></i> Services</a></li>
								<li class="{{str_contains(Request::path(), '/contact-us') ? 'active' : '' }}"><a href="{{url('/dashboard/basic-config/contact-us')}}"><i class="fa fa-map-marker"></i> Contact Us</a></li>
								<li class="{{str_contains(Request::path(), '/social-media') ? 'active' : '' }}"><a href="{{url('/dashboard/basic-config/social-media')}}"><i class="fa fa-facebook-square"></i> Social Media</a></li>
							</ul>
						</li>

						<li class="treeview {{str_contains(Request::path(), 'content') ? 'active' : '' }}">
							<a href="#">
							<i class="fa fa-folder"></i>
							<span>Contents</span>
							<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="{{str_contains(Request::path(), '/features') ? 'active' : '' }}"><a href="{{url('/dashboard/content/features')}}"><i class="fa fa-list-ul"></i> Features</a></li>
								<li class="{{str_contains(Request::path(), '/achievements') ? 'active' : '' }}"><a href="{{url('/dashboard/content/achievements')}}"><i class="fa fa-star"></i> Achievements</a></li>
								<li class="{{str_contains(Request::path(), '/partners') ? 'active' : '' }}"><a href="{{url('/dashboard/content/partners')}}"><i class="fa fa-certificate"></i> Partners</a></li>
								<li class="{{str_contains(Request::path(), '/testimonials') ? 'active' : '' }}"><a href="{{url('/dashboard/content/testimonials')}}"><i class="fa fa-certificate"></i> Testimonials</a></li>
							</ul>
						</li>
						
						<li class="{{str_contains(Request::path(), 'companies') ? 'active' : '' }}" >
							<a  href="{{url('/dashboard/companies')}}">
								<i class="fa fa-building-o"></i> <span>Companies</span>
							</a>
						</li>

						<li class="{{str_contains(Request::path(), 'products') ? 'active' : '' }}" >
							<a  href="{{url('/dashboard/products')}}">
								<i class="fa fa-shopping-bag"></i> <span>Products</span>
							</a>
						</li>

						<li class="{{str_contains(Request::path(), 'categories') ? 'active' : '' }}" >
							<a  href="{{url('/dashboard/categories')}}">
								<i class="fa fa-list-ul"></i> <span>Categories</span>
							</a>
						</li>

						<li class="{{str_contains(Request::path(), 'dashboard/contact-form') ? 'active' : '' }}" >
							<a  href="{{url('/dashboard/contact-form')}}">
								<i class="fa fa-file-text-o"></i> <span>Contact Form</span>
							</a>
						</li>

						<li class="{{str_contains(Request::path(), 'dashboard/media') ? 'active' : '' }}" >
							<a  href="{{url('/dashboard/media')}}">
								<i class="fa fa-picture-o"></i> <span>Media</span>
							</a>
						</li>

						<!--<li class="treeview">
							<a href="#">
							<i class="fa fa-edit"></i> <span>Forms</span>
							<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
							<li><a href="forms.html"><i class="fa fa-angle-right"></i> General Forms</a></li>
							<li><a href="validation.html"><i class="fa fa-angle-right"></i> Form Validations</a></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
							<i class="fa fa-table"></i> <span>Tables</span>
							<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
							<li><a href="tables.html"><i class="fa fa-angle-right"></i> Simple tables</a></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
							<i class="fa fa-envelope"></i> <span>Mailbox </span>
							<i class="fa fa-angle-left pull-right"></i><small class="label pull-right label-info1">08</small><span class="label label-primary1 pull-right">02</span></a>
							<ul class="treeview-menu">
							<li><a href="inbox.html"><i class="fa fa-angle-right"></i> Mail Inbox </a></li>
							<li><a href="compose.html"><i class="fa fa-angle-right"></i> Compose Mail </a></li>
							</ul>
						</li>
						<li class="treeview">
							<a href="#">
							<i class="fa fa-folder"></i> <span>Examples</span>
							<i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
							<li><a href="login.html"><i class="fa fa-angle-right"></i> Login</a></li>
							<li><a href="signup.html"><i class="fa fa-angle-right"></i> Register</a></li>
							<li><a href="404.html"><i class="fa fa-angle-right"></i> 404 Error</a></li>
							<li><a href="500.html"><i class="fa fa-angle-right"></i> 500 Error</a></li>
							<li><a href="blank-page.html"><i class="fa fa-angle-right"></i> Blank Page</a></li>
							</ul>
						</li>
						<li class="header">LABELS</li>
						<li><a href="#"><i class="fa fa-angle-right text-red"></i> <span>Important</span></a></li>
						<li><a href="#"><i class="fa fa-angle-right text-yellow"></i> <span>Warning</span></a></li>
						<li><a href="#"><i class="fa fa-angle-right text-aqua"></i> <span>Information</span></a></li> -->
						</ul>
					</div>
					<!-- /.navbar-collapse -->
				</nav>
			</aside>
		</div>