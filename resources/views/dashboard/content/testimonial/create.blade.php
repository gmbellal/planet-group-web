@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/content')}}">Content</a></li>
			<li><a href="{{url('/dashboard/content/testimonials')}}">Testimonials</a></li>
			<li class="active">Add New</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Add New Testimonial</h4>
				</div>
				
				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')

				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/content/testimonials?page=save')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Client Name</label>
							<div class="col-sm-6">
								<input type="text" name="client_name" value="{{!empty($item->client_name) ? $item->client_name : old('client_name')}}" class="form-control"  placeholder="Client Name" minlength="1" maxlength="50" required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Client name here. Max 50 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Testimonial Body</label>
							<div class="col-sm-6">
								<textarea name="body" id="txtarea" cols="50" rows="6" class="form-control" required >{{!empty($item->body) ? $item->body : old('body')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Testimonial body here.  Max 300 characters</p>
							</div>
						</div>


						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Image</label>
							<div class="col-sm-6">
								<input type="file" name="image" class="form-control" accept="image/png, image/gif, image/jpeg"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 5MB. &nbsp; JPEG/PNG only</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Sequence</label>
							<div class="col-sm-6">
								<input type="number" name="sequence"  value="{{!empty($item->sequence) ? $item->sequence : old('sequence')}}" class="form-control"  placeholder="Sequence" minlength="1" maxlength="2"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Sequence here. Default : 1</p>
							</div>
						</div>
						
						<div class="form-group">
							<label for="radio" class="col-sm-2 control-label">Status</label>
							<div class="col-sm-6">
								<div class="radio-inline"><label><input name="status" type="radio" value="Active" checked=""> Active</label></div>
								<div class="radio-inline"><label><input name="status" type="radio" value="Inactive" > Inactive</label></div>
							</div>
						</div>
						
						<center>
							<button type="submit" class="btn btn-success" style="margin-right: 20px">Submit</button>
							<a href="{{url('/dashboard/content/testimonials')}}" class="btn btn-warning">Cancel</a>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
