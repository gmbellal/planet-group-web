@extends('dashboard.layout.master')

@section('extra_css')
	
@endsection

@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">
		<!-- breadcrumb -->
		<ol class="breadcrumb">
			<li><a href="{{url('/dashboard')}}">Dashboard</a></li>
			<li><a href="{{url('/dashboard/content')}}">Content</a></li>
			<li><a href="{{url('/dashboard/content/features')}}">Features</a></li>
			<li class="active">Add New</li>
		</ol>
				
		<div class="forms">
			
			<div class="form-grids row widget-shadow" data-example-id="basic-forms">
				<div class="form-title">
					<h4>Add New Feature</h4>
				</div>
				
				<!-- validation error -->
				@include('dashboard.layout.common.validation_error')

				<div class="form-body">
					<form class="form-horizontal" action="{{url('/dashboard/content/features?page=save')}}"  method="post" enctype="multipart/form-data" >
						{{ csrf_field() }}
						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Ttile</label>
							<div class="col-sm-6">
								<input type="text" name="title" value="{{!empty($item->title) ? $item->title : old('title')}}" class="form-control"  placeholder="Title" minlength="1" maxlength="50" required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Feature title here. Max 50 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Body</label>
							<div class="col-sm-6">
								<textarea name="body" id="txtarea" cols="50" rows="8" class="form-control" required >{{!empty($item->body) ? $item->body : old('body')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Feature body here</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">URL</label>
							<div class="col-sm-6">
								<input type="text" name="url" value="{{!empty($item->url) ? $item->url : old('url')}}" class="form-control"  placeholder="URL" minlength="5" maxlength="150"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Feature URL here. Max 150 characters</p>
							</div>
						</div>

						<div class="form-group">
							<label for="mediuminput" class="col-sm-2 control-label">Image</label>
							<div class="col-sm-6">
								<input type="file" name="image" class="form-control" accept="image/png, image/gif, image/jpeg"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 5MB. &nbsp; JPEG/PNG only</p>
							</div>
						</div>

						<div class="form-group">
							<label for="title" class="col-sm-2 control-label">Feature Sequence</label>
							<div class="col-sm-6">
								<input type="number" name="feature_sequence"  value="{{!empty($item->feature_sequence) ? $item->feature_sequence : old('feature_sequence')}}" class="form-control"  placeholder="Slider Sequence" minlength="1" maxlength="2"  required>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Feature Sequence here. Default : 1</p>
							</div>
						</div>
						
						<div class="form-group">
							<label for="txtarea1" class="col-sm-2 control-label">Meta Tag</label>
							<div class="col-sm-6">
								<textarea name="meta" id="txtarea" cols="50" rows="2" class="form-control">{{!empty($item->meta) ? $item->meta : old('meta')}}</textarea>
							</div>
							<div class="col-sm-4">
								<p class="help-block">Max. 100 characters</p>
							</div>
						</div>
						
						<div class="form-group">
							<label for="radio" class="col-sm-2 control-label">Status</label>
							<div class="col-sm-6">
								<div class="radio-inline"><label><input name="status" type="radio" value="Active" checked=""> Active</label></div>
								<div class="radio-inline"><label><input name="status" type="radio" value="Inactive" > Inactive</label></div>
							</div>
						</div>
						
						<center>
							<button type="submit" class="btn btn-success" style="margin-right: 20px">Submit</button>
							<a href="{{url('/dashboard/content/features')}}" class="btn btn-warning">Cancel</a>
						</center>
					</form>


				</div>
			</div>

		</div>


   </div>
</div>
@endsection()











@section('extra_js')

<!-- Extra page js-->
<script>
	//js here
</script>

@endsection()
