@extends('dashboard.layout.master')


@section('extra_css')

@endsection



@section('content')
<!-- main content start-->

<div id="page-wrapper">
   <div class="main-page">



      <div class="col_3">
         <div class="col-md-3 widget widget1">
            <div class="r3_counter_box">
               <i class="pull-left fa fa-globe user icon-rounded"></i>
               <div class="stats">
                  <h5><strong>{{$tracker['t_hits']}}</strong></h5>
                  <span>Total Hits</span>
               </div>
            </div>
         </div>
		 <div class="col-md-3 widget widget1">
            <div class="r3_counter_box">
               <i class="pull-left fa fa-internet-explorer dollar1 icon-rounded"></i>
               <div class="stats">
                  <h5><strong>{{$t_visitors}}</strong></h5>
                  <span>Total Visitors</span>
               </div>
            </div>
         </div>
		 <div class="col-md-3 widget widget1">
            <div class="r3_counter_box">
               <i class="pull-left fa fa-envelope user2 icon-rounded"></i>
               <div class="stats">
                  <h5><strong>{{$contact_form}}</strong></h5>
                  <span>Contact Request</span>
               </div>
            </div>
         </div>
         <div class="col-md-3 widget widget1">
            <div class="r3_counter_box">
               <i class="pull-left fa fa-shopping-cart user1 icon-rounded"></i>
               <div class="stats">
                  <h5><strong>{{$t_products}}</strong></h5>
                  <span>Total Products</span>
               </div>
            </div>
         </div>
         
         <div class="col-md-3 widget">
            <div class="r3_counter_box">
               <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
               <div class="stats">
                  <h5><strong>{{$users}}</strong></h5>
                  <span>Total Users</span>
               </div>
            </div>
         </div>
         <div class="clearfix"> </div>
      </div>




      <div class="row-one widgettable">


         <div class="col-md-7 content-top-2 card">
            <div class="agileinfo-cdr">
               <div class="card-header">
                  <h3>Weekly Visitors</h3>
               </div>
               <div id="Linegraph" style="width: 98%; height: 350px">
               </div>
            </div>
         </div>


		 




         <div class="col-md-3 stat">
            <div class="content-top-1">
               <div class="col-md-6 top-content">
                  <h5>Features</h5>
                  <label>{{$t_features}}</label>
               </div>
               <div class="col-md-6 top-content1">
                  <div id="demo-pie-1" class="pie-title-center" data-percent="60"> <span class="pie-value"></span> </div>
               </div>
               <div class="clearfix"> </div>
            </div>
            <div class="content-top-1">
               <div class="col-md-6 top-content">
                  <h5>Companies</h5>
                  <label>{{$t_companies}}</label>
               </div>
               <div class="col-md-6 top-content1">
                  <div id="demo-pie-2" class="pie-title-center" data-percent="75"> <span class="pie-value"></span> </div>
               </div>
               <div class="clearfix"> </div>
            </div>
            <div class="content-top-1">
               <div class="col-md-6 top-content">
                  <h5>Partners</h5>
                  <label>{{$t_partners}}</label>
               </div>
               <div class="col-md-6 top-content1">
                  <div id="demo-pie-3" class="pie-title-center" data-percent="55"> <span class="pie-value"></span> </div>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
         <div class="col-md-2 stat">
            <div class="content-top">
               <div class="top-content facebook">
                  <a href="#"><i class="fa fa-facebook"></i></a>
               </div>
               <ul class="info">
                  <li class="col-md-6">
                     <b>1,296</b>
                     <p>Friends</p>
                  </li>
                  <li class="col-md-6">
                     <b>647</b>
                     <p>Likes</p>
                  </li>
                  <div class="clearfix"></div>
               </ul>
            </div>
            <div class="content-top">
               <div class="top-content twitter">
                  <a href="#"><i class="fa fa-twitter"></i></a>
               </div>
               <ul class="info">
                  <li class="col-md-6">
                     <b>1,997</b>
                     <p>Followers</p>
                  </li>
                  <li class="col-md-6">
                     <b>389</b>
                     <p>Tweets</p>
                  </li>
                  <div class="clearfix"></div>
               </ul>
            </div>
            <div class="content-top">
               <div class="top-content google-plus">
                  <a href="#"><i class="fa fa-google-plus"></i></a>
               </div>
               <ul class="info">
                  <li class="col-md-6">
                     <b>1,216</b>
                     <p>Followers</p>
                  </li>
                  <li class="col-md-6">
                     <b>321</b>
                     <p>shares</p>
                  </li>
                  <div class="clearfix"></div>
               </ul>
            </div>
         </div>
         <div class="clearfix"> </div>
      </div>






	  <div class="row-one widgettable">


         <div class="col-md-12 content-top-2 card">
            <div class="agileinfo-cdr">
               <div class="card-header">
                  <h3>Visitor Logs</h3><br>
               </div>
               <div  style="width: 100%; height: 350px;">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Date & Time</th>
								<th>IP Address</th>
								<th class="text-center">Total Hits</th>
								<th>Browser/Device</th>
							</tr>
						</thead>
						<tbody>
							@php $i = 1  @endphp
							@foreach($visitor_log as $list)
							<tr>
								<th scope="row">{{$i++}}</th>
								<td>{{$list->date}} {{$list->visit_time}}</td>
								<td>{{$list->ip}}</td>
								<td class="text-center">{{$list->hits}}</td>
								<td>{{$list->agent}}</td>
							
							</tr>
							@endforeach
						</tbody>
					</table>

				</div>
            </div>
         </div>


		
         <div class="clearfix"> </div>
      </div>


   </div>
</div>



@endsection()











@section('extra_js')




<!-- for index page weekly sales java script -->
<script>
	var graphdata1 = {
		linecolor: "#CCA300",
		title: "Monday",
		values: [
		{ X: "6:00", Y: 10.00 },
		{ X: "7:00", Y: 20.00 },
		{ X: "8:00", Y: 40.00 },
		{ X: "9:00", Y: 34.00 },
		{ X: "10:00", Y: 40.25 },
		{ X: "11:00", Y: 28.56 },
		{ X: "12:00", Y: 18.57 },
		{ X: "13:00", Y: 34.00 },
		{ X: "14:00", Y: 40.89 },
		{ X: "15:00", Y: 12.57 },
		{ X: "16:00", Y: 28.24 },
		{ X: "17:00", Y: 18.00 },
		{ X: "18:00", Y: 34.24 },
		{ X: "19:00", Y: 40.58 },
		{ X: "20:00", Y: 12.54 },
		{ X: "21:00", Y: 28.00 },
		{ X: "22:00", Y: 18.00 },
		{ X: "23:00", Y: 34.89 },
		{ X: "0:00", Y: 40.26 },
		{ X: "1:00", Y: 28.89 },
		{ X: "2:00", Y: 18.87 },
		{ X: "3:00", Y: 34.00 },
		{ X: "4:00", Y: 40.00 }
		]
	};
	var graphdata2 = {
		linecolor: "#00CC66",
		title: "Tuesday",
		values: [
		{ X: "6:00", Y: 100.00 },
		{ X: "7:00", Y: 120.00 },
		{ X: "8:00", Y: 140.00 },
		{ X: "9:00", Y: 134.00 },
		{ X: "10:00", Y: 140.25 },
		{ X: "11:00", Y: 128.56 },
		{ X: "12:00", Y: 118.57 },
		{ X: "13:00", Y: 134.00 },
		{ X: "14:00", Y: 140.89 },
		{ X: "15:00", Y: 112.57 },
		{ X: "16:00", Y: 128.24 },
		{ X: "17:00", Y: 118.00 },
		{ X: "18:00", Y: 134.24 },
		{ X: "19:00", Y: 140.58 },
		{ X: "20:00", Y: 112.54 },
		{ X: "21:00", Y: 128.00 },
		{ X: "22:00", Y: 118.00 },
		{ X: "23:00", Y: 134.89 },
		{ X: "0:00", Y: 140.26 },
		{ X: "1:00", Y: 128.89 },
		{ X: "2:00", Y: 118.87 },
		{ X: "3:00", Y: 134.00 },
		{ X: "4:00", Y: 180.00 }
		]
	};
	var graphdata3 = {
		linecolor: "#FF99CC",
		title: "Wednesday",
		values: [
		{ X: "6:00", Y: 230.00 },
		{ X: "7:00", Y: 210.00 },
		{ X: "8:00", Y: 214.00 },
		{ X: "9:00", Y: 234.00 },
		{ X: "10:00", Y: 247.25 },
		{ X: "11:00", Y: 218.56 },
		{ X: "12:00", Y: 268.57 },
		{ X: "13:00", Y: 274.00 },
		{ X: "14:00", Y: 280.89 },
		{ X: "15:00", Y: 242.57 },
		{ X: "16:00", Y: 298.24 },
		{ X: "17:00", Y: 208.00 },
		{ X: "18:00", Y: 214.24 },
		{ X: "19:00", Y: 214.58 },
		{ X: "20:00", Y: 211.54 },
		{ X: "21:00", Y: 248.00 },
		{ X: "22:00", Y: 258.00 },
		{ X: "23:00", Y: 234.89 },
		{ X: "0:00", Y: 210.26 },
		{ X: "1:00", Y: 248.89 },
		{ X: "2:00", Y: 238.87 },
		{ X: "3:00", Y: 264.00 },
		{ X: "4:00", Y: 270.00 }
		]
	};
	var graphdata4 = {
		linecolor: "Random",
		title: "Thursday",
		values: [
		{ X: "6:00", Y: 300.00 },
		{ X: "7:00", Y: 410.98 },
		{ X: "8:00", Y: 310.00 },
		{ X: "9:00", Y: 314.00 },
		{ X: "10:00", Y: 310.25 },
		{ X: "11:00", Y: 318.56 },
		{ X: "12:00", Y: 318.57 },
		{ X: "13:00", Y: 314.00 },
		{ X: "14:00", Y: 310.89 },
		{ X: "15:00", Y: 512.57 },
		{ X: "16:00", Y: 318.24 },
		{ X: "17:00", Y: 318.00 },
		{ X: "18:00", Y: 314.24 },
		{ X: "19:00", Y: 310.58 },
		{ X: "20:00", Y: 312.54 },
		{ X: "21:00", Y: 318.00 },
		{ X: "22:00", Y: 318.00 },
		{ X: "23:00", Y: 314.89 },
		{ X: "0:00", Y: 310.26 },
		{ X: "1:00", Y: 318.89 },
		{ X: "2:00", Y: 518.87 },
		{ X: "3:00", Y: 314.00 },
		{ X: "4:00", Y: 310.00 }
		]
	};



	$(function () {
	
		
		$("#sltchartype").on('change', function () {
			$("#Bargraph").SimpleChart('ChartType', $(this).val());
			$("#Bargraph").SimpleChart('reload', 'true');
		});

		
		$("#Linegraph").SimpleChart({
			ChartType: "Line",
			toolwidth: "50",
			toolheight: "25",
			axiscolor: "#E6E6E6",
			textcolor: "#6E6E6E",
			showlegends: false,
			data: [graphdata4, graphdata3, graphdata2, graphdata1],
			legendsize: "140",
			legendposition: 'bottom',
			xaxislabel: 'Hours',
			title: 'Visitor',
			yaxislabel: 'Visitor In Number'
		});
		
		
	});

</script>
<!-- //for index page weekly sales java script -->
@endsection()
