<?php

//Web Site Routes
Route::get('/',  'Homepage\HomepageController@Index');
Route::any('/home',  'Homepage\HomepageController@Index');
Route::get('/about',  'Homepage\HomepageController@About');
Route::any('/contact',  'Homepage\HomepageController@Contact');
Route::get('/company/{id}',  'Homepage\HomepageController@Company');
Route::get('/product/category/{id}',  'Homepage\HomepageController@ProductCategory');
Route::get('/product/details/{id}',  'Homepage\HomepageController@ProductDetails');


//Auth
Route::get('login', 'Auth\LoginController@login')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('dashboard/logout', 'Auth\LoginController@logout')->name('logout');







// After login this route group will execute
Route::middleware(['auth'])->group(function () {

  //Dashboard Routes
  Route::get('/dashboard',  'Dashboard\DashboardController@index');

  //Config Routes
  Route::get('/dashboard/basic-config',  'Dashboard\ConfigController@index');
  Route::get('/dashboard/basic-config/brand-logo',  'Dashboard\ConfigController@BrandLogo');
  Route::any('/dashboard/basic-config/home-slider',  'Dashboard\ConfigController@HomeSlider');
  Route::any('/dashboard/basic-config/about-us',  'Dashboard\ConfigController@AboutUs');
  Route::any('/dashboard/basic-config/services',  'Dashboard\ConfigController@Services');
  Route::any('/dashboard/basic-config/contact-us',  'Dashboard\ConfigController@ContactUs');
  Route::any('/dashboard/basic-config/social-media',  'Dashboard\ConfigController@SocialMedia');


  //Content Routes
  Route::any('/dashboard/content/features',      'Dashboard\ContentController@Features');
  Route::any('/dashboard/content/achievements',  'Dashboard\ContentController@Achievements');
  Route::any('/dashboard/content/partners',      'Dashboard\ContentController@Partners');
  Route::any('/dashboard/content/testimonials',  'Dashboard\ContentController@Testimonials');

  //Company Routes
  Route::any('/dashboard/companies',      'Dashboard\CompanyController@Companies');

  //Product Routes
  Route::any('/dashboard/products',      'Dashboard\ProductController@Products');


  //Category Routes
  Route::any('/dashboard/categories',      'Dashboard\CategoryController@Categories');


  //contact-form
  Route::get('/dashboard/contact-form',      'Dashboard\DashboardController@ContactForm');
  Route::get('/dashboard/media',      'Dashboard\DashboardController@Media');

  
  //My Account
  Route::any('/dashboard/account',                  'Dashboard\AccountController@index');
  Route::any('/dashboard/account/profile',          'Dashboard\AccountController@Profile');
  Route::any('/dashboard/account/change-password',  'Dashboard\AccountController@ChangePassword');

  Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
  });


});

