<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\Slider;
use App\Model\ContactUs;
use App\Model\AboutUs;
use App\Model\Services;
use App\Model\SocialMedia;



class ConfigController extends Controller{


    public function __construct(){
        //construct here


    }


    public function index(){
       
        $data['slider'] =  Slider::get();
        //dd($data);
        return view('dashboard.index',$data);
    }


    
    public function BrandLogo(Request $request){
        if($request->input('page')==''){
            $data['sliders'] =  Slider::get();
            return view('dashboard.basic-config.logo.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.basic-config.logo.create');
        }else if($request->input('page')=='save'){
            
            $validate = $this->validate($request,[
                'title'     =>'required|min:1|max:5',
                'url'       =>'required|min:5|max:150',
                'status'    =>'required',
                'photo'     => 'mimes:jpg,bmp,png',
                'sl_no'     =>'required|min:1|max:2'
            ]);

            //save here
            //dd($request->input());
        }else if($request->input('page')=='edit'){
            //edit view here
           
        }else if($request->input('page')=='update'){
            //update here
           
        }else if($request->input('page')=='delete'){
            //delete here
            
        }
        
     

       
    }





    public function HomeSlider(Request $request){
        if($request->input('page')==''){
            $data['sliders'] =  Slider::orderBy('slider_sequence','asc')->get();
            return view('dashboard.basic-config.slider.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.basic-config.slider.create');
        }else if($request->input('page')=='save'){
             //save here
            $validate = $this->validate($request,[
                'title'     =>'required|min:1|max:50',
                'url'       =>'required|min:5|max:150',
                'status'    =>'required',
                'image'     => 'required|mimes:jpeg,png,jpg,JPG|max:5120',
                'slider_sequence'     =>'required|min:1|max:2',
                'sub_image_1' => 'required_with:sub_title_1|mimes:jpeg,png,jpg,JPG | max:5120',
                'sub_image_2' => 'required_with:sub_title_2|mimes:jpeg,png,jpg,JPG | max:5120',
                'sub_title_1' => 'required_with:sub_image_1|max:50',
                'sub_url_1'   =>  'max:150',
                'sub_title_2' =>  'required_with:sub_image_2|max:50',
                'sub_url_2'   =>  'max:150',
              
            ]);
            //dd($request->input());
            $newSlider = new Slider();
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newSlider->image = $newfilename;
            }
            if($request->hasFile('sub_image_1')){
                $source_file = $request->file('sub_image_1');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newSlider->sub_image_1 = $newfilename;
            }
            if($request->hasFile('sub_image_2')){
                $source_file = $request->file('sub_image_2');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newSlider->sub_image_2 = $newfilename;
            }

            $newSlider->title = $request->input('title');
            $newSlider->url = $request->input('url');
            $newSlider->slider_sequence = $request->input('slider_sequence');
            $newSlider->status = $request->input('status');
            $newSlider->sub_title_1 = $request->input('sub_title_1');
            $newSlider->sub_title_2 = $request->input('sub_title_2');
            $newSlider->sub_url_1 = $request->input('sub_url_1');
            $newSlider->sub_url_2 = $request->input('sub_url_2');
            $newSlider->created_by = Auth::user()->id;
            $newSlider->save();
            
             $msg = "Slider Added";
             return redirect(url('/dashboard/basic-config/home-slider'))->with('error',$msg);

        }else if($request->input('page')=='edit'){
            //edit view here
           
        }else if($request->input('page')=='update'){
            //update here
           
        }else if($request->input('page')=='status'){
            //update here
            $slider = Slider::findOrFail($request->input('id'));
            if($request->input('ref')=='Active'){
                $slider->status = 'Inactive';
            }else{
                $slider->status = 'Active';
            }
            $slider->save();
            $msg = "Action Done";
            return redirect(url('/dashboard/basic-config/home-slider'))->with('error',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $slider = Slider::findOrFail($request->input('id'));
                if($slider->image){
                    if(file_exists("uploads/".$slider->image)){
                        unlink("uploads/".$slider->image);
                    }
                }
                if($slider->sub_image_1){
                    if(file_exists("uploads/".$slider->sub_image_1)){
                        unlink("uploads/".$slider->sub_image_1);
                    }

                }
                if($slider->sub_image_2){
                    if(file_exists("uploads/".$slider->sub_image_2)){
                        unlink("uploads/".$slider->sub_image_2);
                    }
                }
                $is_delete = Slider::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/basic-config/home-slider'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }







    public function AboutUs(Request $request){
        if($request->input('page')==''){
            $data['about'] = AboutUs::get()->first();
            return view('dashboard.basic-config.about-us.index',$data);
        }else if($request->input('page')=='update'){
            $validate = $this->validate($request,[
                'title'     =>'required|min:3|max:50',
                'body'       =>'required|min:10',
                'url'       =>'max:150',
                'image'     => 'required_without:old_image|mimes:jpeg,png,jpg,JPG | max:5120',
                'meta'       =>'max:150'
            ]);
            $updateAboutUs = AboutUs::get()->first();
            if(empty($updateAboutUs)){
                $updateAboutUs = new AboutUs;
                $updateAboutUs->created_by = Auth::user()->id;
            }else{
                $updateAboutUs->updated_by = Auth::user()->id;
            }

            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $updateAboutUs->image = $newfilename;
                $new_image = $newfilename;
            }
            $updateAboutUs->title = $request->input('title');
            $updateAboutUs->body = $request->input('body');
            $updateAboutUs->url = $request->input('url');
            $updateAboutUs->meta = $request->input('meta');
            $is_saved = $updateAboutUs->save();

            //dd($request->input('old_image'));

            if(!empty($new_image)){
                if(!empty($request->input('old_image')) && file_exists("uploads/".$request->input('old_image'))){
                    unlink("uploads/".$request->input('old_image'));
                }
            }

            $msg = "About Us Updated";
            return redirect(url('/dashboard/basic-config/about-us'))->with('success',$msg);
        }else{
            echo "Invalid Request";
            exit();
        }
    }




    public function Services(Request $request){
        if($request->input('page')==''){
            $data['services'] = Services::get()->first();
            return view('dashboard.basic-config.services.index',$data);
        }else if($request->input('page')=='update'){
            
            $updateServices = Services::get()->first();

            $validate = $this->validate($request,[
                'title'     =>'required|min:3|max:50',
                'body'      =>'required|min:10',
                'services'  =>'required|min:3|max:500',
                'meta'      =>'max:100',
                'image'     => 'required_without:old_image|mimes:jpeg,png,jpg,JPG | max:5120',
                'meta'      =>'max:150'
            ]);

            if(empty($updateServices)){
                $updateServices = new Services;
                $updateServices->created_by = Auth::user()->id;
            }else{
                $updateServices->updated_by = Auth::user()->id;
            }

            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $updateServices->image = $newfilename;
                $new_image = $newfilename;
            }
            $updateServices->title = $request->input('title');
            $updateServices->body = $request->input('body');
            $updateServices->services = $request->input('services');
            $updateServices->meta = $request->input('meta');
            $is_saved = $updateServices->save();
            if(!empty($new_image)){
                if(!empty($request->input('old_image')) && file_exists("uploads/".$request->input('old_image'))){
                    unlink("uploads/".$request->input('old_image'));
                }
            }
            $msg = "Services Updated";
            return redirect(url('/dashboard/basic-config/services'))->with('success',$msg);
        }else{
            echo "Invalid Request";
            exit();
        }
    }







    public function ContactUs(Request $request){
        if($request->input('page')==''){
            $data['contact'] = ContactUs::get()->first();
            return view('dashboard.basic-config.contact-us.index',$data);
        }else if($request->input('page')=='update'){
            $validate = $this->validate($request,[
                'email'     =>'required|min:5|max:50',
                'phone'       =>'required|min:10|max:15',
                'address'       =>'required|min:5|max:150'
            ]);
            $updateContactUs = ContactUs::get()->first();
            if(empty($updateContactUs)){
                $updateContactUs = new ContactUs;
                $updateContactUs->created_by = Auth::user()->id;
            }else{
                $updateContactUs->updated_by = Auth::user()->id;
            }
            $updateContactUs->email = $request->input('email');
            $updateContactUs->phone = $request->input('phone');
            $updateContactUs->address = $request->input('address');
            $updateContactUs->save();
            $msg = "Contact Us Updated";
            return redirect(url('/dashboard/basic-config/contact-us'))->with('error',$msg);
        }else{
            echo "Invalid Request";
            exit();
        }
    }



    public function SocialMedia(Request $request){
        if($request->input('page')==''){
            $data['social_media'] = SocialMedia::get()->first();
            return view('dashboard.basic-config.social-media.index',$data);
        }else if($request->input('page')=='update'){
            $validate = $this->validate($request,[
                'facebook'     =>'required|min:1|max:200',
                'twitter'       =>'required|min:1|max:200',
                'instagram'       =>'required|min:1|max:200',
                'linkedin'       =>'required|min:1|max:200',
            ]);
            $updateSocialMedia = SocialMedia::get()->first();
            if(empty($updateSocialMedia)){
                $updateSocialMedia = new SocialMedia;
                $updateSocialMedia->created_by = Auth::user()->id;
            }else{
                $updateSocialMedia->updated_by = Auth::user()->id;
            }
            $updateSocialMedia->facebook = $request->input('facebook');
            $updateSocialMedia->twitter = $request->input('twitter');
            $updateSocialMedia->instagram = $request->input('instagram');
            $updateSocialMedia->linkedin = $request->input('linkedin');
            $updateSocialMedia->save();
            $msg = "Social Media Updated";
            return redirect(url('/dashboard/basic-config/social-media'))->with('error',$msg);
        }else{
            echo "Invalid Request";
            exit();
        }
    }









  






}