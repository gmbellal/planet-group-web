<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\ContactForm;
use App\Model\Products;
use App\Model\Tracker;
use App\Model\User;
use App\Model\Categories;
use App\Model\Features;
use App\Model\Companies;
use App\Model\Partners;
use File;



class DashboardController extends Controller{
    public function __construct(){
        //add code here
    }


    public function index(){
        $data['title'] = 'Planet Group';
        $data['t_products'] = Products::count();
        $data['contact_form'] = ContactForm::count();
        $data['users'] = User::count();
        $data['categories'] = Categories::count();
        $data['tracker'] = Tracker::select(DB::raw('SUM(tracker.hits) AS t_hits'))->first()->toArray();
        $data['t_visitors'] = Tracker::count();
        $data['t_features'] = Features::count();
        $data['t_companies'] = Companies::count();
        $data['t_partners'] = Partners::count();
        $data['visitor_log'] = Tracker::orderBy('updated_at', 'desc')->take(5)->get();
        //dd($data);
        return view('dashboard.index',$data);
    }



    public function ContactForm(){
        $data['title'] = 'Planet Group';
        $data['contact_form'] =  ContactForm::get();
        return view('dashboard.contact-form.index',$data);
    }



    public function Media(){
        $data['title'] = 'Planet Group';
        $data['files'] = File::allFiles(base_path('uploads'));
        
        //dd(  $data['files'] );

        return view('dashboard.media.index',$data);
    }

    






}