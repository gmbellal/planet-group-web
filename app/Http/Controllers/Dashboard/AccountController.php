<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\User;



class AccountController extends Controller{


    public function __construct(){
        //construct here
    }

    public function index(){
        $data['title'] = '';
        return view('dashboard.account.index',$data);
    }


    
    public function Profile(Request $request){
        if($request->input('page')==''){
            $data['profile'] =  User::findOrFail(Auth::user()->id);
            return view('dashboard.account.profile.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.basic-config.logo.create');
        }else if($request->input('page')=='save'){
            //save here
        }else if($request->input('page')=='edit'){
            //edit view here
           
        }else if($request->input('page')=='update'){
            //update here
            $validate = $this->validate($request,[
                'fullname'  =>'required|min:3|max:50',
                'image'     => 'mimes:jpeg,png,jpg,JPG|max:1024'
            ]);
            $updateProfile = User::findOrFail(Auth::user()->id);
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $updateProfile->image = $newfilename;
                $new_image = $newfilename;
            }
            $updateProfile->fullname = $request->input('fullname');
            $updateProfile->updated_by = Auth::user()->id;
            $updateProfile->save();
            if(!empty($new_image)){
                if(!empty($request->input('old_image')) && file_exists("uploads/".$request->input('old_image'))){
                    unlink("uploads/".$request->input('old_image'));
                }
            }
            $msg = "Profile Updated";
            return redirect(url('/dashboard/account/profile'))->with('success',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            
        }
        
       
    }




    public function ChangePassword(Request $request){
        if($request->input('page')==''){
            $data['profile'] =  User::findOrFail(Auth::user()->id);
            return view('dashboard.account.password.index',$data);
        }else if($request->input('page')=='update'){
            //update here
            $user =  User::findOrFail(Auth::user()->id);
            $validate = $this->validate($request,[
                'current_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                    if ( md5($value) != $user->password ) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }],
                'new_password' => 'required|different:current_password',
                'confirm_new_password' => 'required|same:new_password'
            ]);

            $user->password = md5($request->input('new_password'));
            $user->last_pasword_updated_at = date('Y-m-d H:m:i');
            $user->updated_by = Auth::user()->id;
            $user->save();            

            $msg = "Password Changed";
            return redirect(url('/dashboard/account'))->with('success',$msg);
        }else{
            echo "Invalid Request";
            exit();
        }
        
     

       
    }











  






}