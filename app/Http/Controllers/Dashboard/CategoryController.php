<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\Categories;
use App\Model\Companies;




class CategoryController extends Controller{


    public function __construct(){
        //construct here


    }

    public function index(){
       
    }


    public function Categories(Request $request){
        if($request->input('page')==''){
            $data['categories'] =  Categories::select('categories.*','companies.company_name','pcat.category_name as parent')
                ->leftjoin('companies', 'companies.id', '=', 'categories.company_id')
                ->leftjoin('categories as pcat', 'pcat.id', '=', 'categories.parent_id')
                ->orderBy('categories.sequence','asc')
                ->orderBy('companies.sequence','asc')
                ->get();
            return view('dashboard.category.index',$data);
        }else if($request->input('page')=='create'){
            $data['companies'] =  Companies::orderBy('sequence','asc')->get();
            $categories = Categories::where('parent_id', NULL)
                                ->select('categories.*', 'companies.id as com_id', 'companies.company_name')
                                ->leftjoin('companies', 'companies.id', '=', 'categories.company_id')
                                ->orderBy('sequence','asc')
                                ->get();

           $data['categories'] = group_by($categories,'com_id');
           return view('dashboard.category.create' ,$data);
        }else if($request->input('page')=='save'){
            
            //save here
            $validate = $this->validate($request,[
                'company_id'     =>'required|min:1|max:11',
                'category_name'   =>'required|min:2|max:50',
                'image'     => 'mimes:jpeg,png,jpg,JPG | max:5120',
                'sequence'  =>'required|min:1|max:2'
            ]);

            $newCategory = new Categories();
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newCategory->image = $newfilename;
            }
            
            if($request->input('parent_id')){
                $newCategory->company_id = null;
            }else{
                $newCategory->company_id = $request->input('company_id');
            }
            $newCategory->parent_id = $request->input('parent_id');
            $newCategory->category_name = $request->input('category_name');
            $newCategory->sequence = $request->input('sequence');
            $newCategory->created_by = Auth::user()->id;
            $newCategory->save();
            
            $msg = "Category Added";
            return redirect(url('/dashboard/categories'))->with('success',$msg);

        }else if($request->input('page')=='edit'){
            $data['item'] = Categories::findOrFail($request->input('id'));
            $data['companies'] =  Companies::orderBy('sequence','asc')->get();
            $categories = Categories::where('parent_id', NULL)
                                ->select('categories.*', 'companies.id as com_id', 'companies.company_name')
                                ->leftjoin('companies', 'companies.id', '=', 'categories.company_id')
                                ->orderBy('sequence','asc')
                                ->get();
           $data['categories'] = group_by($categories,'com_id');
           return view('dashboard.category.edit' ,$data);
        }else if($request->input('page')=='update'){
            //update here
            $validate = $this->validate($request,[
                'company_id'     =>'required|min:1|max:11',
                'category_name'   =>'required|min:2|max:50',
                'image'     => 'mimes:jpeg,png,jpg,JPG | max:5120',
                'sequence'  =>'required|min:1|max:2'
            ]);

            $updateCategory = Categories::findOrFail($request->input('id'));

            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $updateCategory->image = $newfilename;
                $new_image = $newfilename;
            }
            if($request->input('parent_id')){
                $updateCategory->company_id = null;
            }else{
                $updateCategory->company_id = $request->input('company_id');
            }
            
            $updateCategory->parent_id = $request->input('parent_id');
            $updateCategory->category_name = $request->input('category_name');
            $updateCategory->sequence = $request->input('sequence');
            $updateCategory->title_color = $request->input('title_color');
            $updateCategory->bg_color = $request->input('bg_color');
            $updateCategory->updated_by = Auth::user()->id;
            $is_update = $updateCategory->save();

            if($is_update){
                if(!empty($new_image)){
                    if(!empty($request->input('old_image')) && file_exists("uploads/".$request->input('old_image'))){
                        unlink("uploads/".$request->input('old_image'));
                    }
                }
                $msg = "Category Updated";
                return redirect(url('/dashboard/categories'))->with('success',$msg);
            }else{
                echo "Opps! Something went Wrong. Please try later";
                exit();
            }
            
        }else if($request->input('page')=='status'){
           
            //do stuffs

        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $Category = Categories::findOrFail($request->input('id'));
                if($Category->image){
                    if(file_exists("uploads/".$Category->image)){
                        unlink("uploads/".$Category->image);
                    }
                }
                $is_delete = Categories::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/categories'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }







}