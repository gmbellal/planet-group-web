<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\Products;
use App\Model\Companies;
use App\Model\Partners;
use App\Model\Categories;




class ProductController extends Controller{


    public function __construct(){
        //construct here


    }

    public function index(){
       
    }


    public function Products(Request $request){
        if($request->input('page')==''){
            $data['products'] =  Products::select('products.*','companies.company_name')
                ->leftjoin('companies', 'companies.id', '=', 'products.company_id')
                ->orderBy('products.sequence','asc')
                ->orderBy('companies.sequence','asc')
                ->get();
            return view('dashboard.product.index',$data);



        }else if($request->input('page')=='create'){
            $data['companies'] =  Companies::orderBy('sequence','asc')->get();
            $categories = Categories::where('parent_id', NULL)
                ->select('categories.*', 'companies.id as com_id', 'companies.company_name')
                ->leftjoin('companies', 'companies.id', '=', 'categories.company_id')
                ->orderBy('sequence','asc')
                ->get();
            $sub_cat = Categories::where('categories.parent_id', '!=', NULL)
                ->select('categories.*', 'pcat.category_name as parent_name')
                ->leftjoin('categories as pcat', 'pcat.id', '=', 'categories.parent_id')
                ->get();

            $categories = group_by($categories,'company_name');
            $sub_cat = group_by($sub_cat,'parent_name');
            
            foreach($categories as $key1 => $plist ){
                foreach( $plist as $key2 =>  $list ){
                    foreach( $sub_cat as $key3 =>  $slist ){
                        if($key3 == $list->category_name){
                            $categories[$key1][$key2] = $sub_cat[$key3];
                        }
                    }
                }
            }
            //dd($categories);
            $data['categories'] = $categories;
            return view('dashboard.product.create' ,$data);
        }else if($request->input('page')=='save'){
             //save here
            $validate = $this->validate($request,[
                'category_id'     =>'required|min:1|max:11',
                'short_description'   =>'required|min:5|max:60',
                'product_name'   =>'required|min:5|max:50',
                'description'   =>'required|min:5|max:5000',
                'meta'       =>'max:100',
                'image'     => 'required|mimes:jpeg,png,jpg,JPG | max:5120',
                'sequence'  =>'required|min:1|max:2'
            ]);

            $catInfo = Categories::find($request->input('category_id'));
            if($catInfo->parent_id){
                $company_id = Categories::find($catInfo->parent_id)->company_id;
            }else{
                $company_id = $catInfo->company_id;
            }

            $newProduct = new Products();
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newProduct->image = $newfilename;
            }
            
            $newProduct->company_id = $company_id;
            $newProduct->category_id = $request->input('category_id');
            $newProduct->product_name = $request->input('product_name');
            $newProduct->description = $request->input('description');
            $newProduct->meta = $request->input('meta');
            $newProduct->sequence = $request->input('sequence');
            $newProduct->status = $request->input('status');
            $newProduct->created_by = Auth::user()->id;
            $newProduct->save();
            
            $msg = "Product Added";
            return redirect(url('/dashboard/products'))->with('success',$msg);

        }else if($request->input('page')=='edit'){
            $data['companies'] =  Companies::orderBy('sequence','asc')->get();
            $data['item'] = Products::findOrFail($request->input('id'));
            $categories = Categories::where('parent_id', NULL)
                ->select('categories.*', 'companies.id as com_id', 'companies.company_name')
                ->leftjoin('companies', 'companies.id', '=', 'categories.company_id')
                ->orderBy('sequence','asc')
                ->get();
            $sub_cat = Categories::where('categories.parent_id', '!=', NULL)
                ->select('categories.*', 'pcat.category_name as parent_name')
                ->leftjoin('categories as pcat', 'pcat.id', '=', 'categories.parent_id')
                ->get();

            $categories = group_by($categories,'company_name');
            $sub_cat = group_by($sub_cat,'parent_name');
            
            foreach($categories as $key1 => $plist ){
                foreach( $plist as $key2 =>  $list ){
                    foreach( $sub_cat as $key3 =>  $slist ){
                        if($key3 == $list->category_name){
                            $categories[$key1][$key2] = $sub_cat[$key3];
                        }
                    }
                }
            }
            $data['categories'] = $categories;
            return view('dashboard.product.edit',$data);
        }else if($request->input('page')=='update'){
        
            //update here
            $validate = $this->validate($request,[
                'category_id'     =>'required|min:1|max:11',
                'product_name'   =>'required|min:5|max:50',
                'short_description'   =>'required|min:5|max:60',
                'description'   =>'required|min:5|max:5000',
                'meta'       =>'max:100',
                'mimes:jpeg,png,jpg,JPG | max:5120',
                'sequence'  =>'required|min:1|max:2'
            ]);

            $catInfo = Categories::find($request->input('category_id'));
            if($catInfo->parent_id){
                $company_id = Categories::find($catInfo->parent_id)->company_id;
            }else{
                $company_id = $catInfo->company_id;
            }

            $updateProduct = Products::findOrFail($request->input('id'));

            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $updateProduct->image = $newfilename;
                $new_image = $newfilename;
            }

            $updateProduct->company_id = $company_id;
            $updateProduct->category_id = $request->input('category_id');
            $updateProduct->product_name = $request->input('product_name');
            $updateProduct->short_description = $request->input('short_description');
            $updateProduct->description = $request->input('description');
            $updateProduct->sequence = $request->input('sequence');
            $updateProduct->status = $request->input('status');
            $updateProduct->updated_by = Auth::user()->id;
            $is_update = $updateProduct->save();

            if($is_update){
                if(!empty($new_image)){
                    if(!empty($request->input('old_image')) && file_exists("uploads/".$request->input('old_image'))){
                        unlink("uploads/".$request->input('old_image'));
                    }
                }
                $msg = "Product Updated";
                return redirect(url('/dashboard/products'))->with('success',$msg);
            }else{
                echo "Opps! Something went Wrong. Please try later";
                exit();
            }


           
        }else if($request->input('page')=='status'){
            //update here
            $product = Products::findOrFail($request->input('id'));
            if($request->input('ref')=='Active'){
                $product->status = 'Inactive';
            }else{
                $product->status = 'Active';
            }
            $product->save();
            $msg = "Action Done";
            return redirect(url('/dashboard/products'))->with('success',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $product = Products::findOrFail($request->input('id'));
                if($product->image){
                    if(file_exists("uploads/".$product->image)){
                        unlink("uploads/".$product->image);
                    }
                }
                $is_delete = Products::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/products'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }







}