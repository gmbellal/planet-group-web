<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\Features;
use App\Model\Achievements;
use App\Model\Testimonials;
use App\Model\Partners;



class ContentController extends Controller{


    public function __construct(){
        //construct here


    }


    public function index(){
       
        
    }


    


    public function Features(Request $request){
        if($request->input('page')==''){
            $data['features'] =  Features::orderBy('feature_sequence','asc')->get();
            return view('dashboard.content.feature.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.content.feature.create');
        }else if($request->input('page')=='save'){
             //save here
            $validate = $this->validate($request,[
                'title'     =>'required|min:1|max:50',
                'body'     =>'required|min:1|max:5000',
                'url'       =>'required|min:5|max:150',
                'status'    =>'required',
                'image'     => 'required|mimes:jpeg,png,jpg,JPG | max:5120',
                'feature_sequence'     =>'required|min:1|max:2'
            ]);

            $newFeature = new Features();
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newFeature->image = $newfilename;
            }
            
            $newFeature->title = $request->input('title');
            $newFeature->body = $request->input('body');
            $newFeature->url = $request->input('url');
            $newFeature->feature_sequence = $request->input('feature_sequence');
            $newFeature->status = $request->input('status');
            $newFeature->meta = $request->input('meta');
            $newFeature->created_by = Auth::user()->id;
            $newFeature->save();
            
             $msg = "Feature Added";
             return redirect(url('/dashboard/content/features'))->with('success',$msg);

        }else if($request->input('page')=='edit'){
            //edit view here
            $data['item'] = Features::findOrFail($request->input('id'));
            return view('dashboard.content.feature.edit',$data);
           
        }else if($request->input('page')=='update'){
            //update here
            $validate = $this->validate($request,[
                'title'     =>'required|min:1|max:50',
                'body'     =>'required|min:1|max:5000',
                'url'       =>'required|min:5|max:150',
                'status'    =>'required',
                'image'     => 'mimes:jpeg,png,jpg,JPG | max:5120',
                'feature_sequence'     =>'required|min:1|max:2'
            ]);
            $updateFeatures = Features::findOrFail($request->input('id'));

            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $updateFeatures->image = $newfilename;
                $new_image = $newfilename;
            }

            $updateFeatures->title = $request->input('title');
            $updateFeatures->body = $request->input('body');
            $updateFeatures->url = $request->input('url');
            $updateFeatures->status = $request->input('status');
            $updateFeatures->feature_sequence = $request->input('feature_sequence');
            $updateFeatures->meta = $request->input('meta');
            $updateFeatures->updated_by = Auth::user()->id;
            $is_update = $updateFeatures->save();

            if($is_update){
                if(!empty($new_image)){
                    if(!empty($request->input('old_image')) && file_exists("uploads/".$request->input('old_image'))){
                        unlink("uploads/".$request->input('old_image'));
                    }
                }
                $msg = "Features Updated";
                return redirect(url('/dashboard/content/features'))->with('success',$msg);
            }else{
                echo "Opps! Something went Wrong. Please try later";
                exit();
            }
           
        }else if($request->input('page')=='status'){
            //update here
            $feature = Features::findOrFail($request->input('id'));
            if($request->input('ref')=='Active'){
                $feature->status = 'Inactive';
            }else{
                $feature->status = 'Active';
            }
            $feature->save();
            $msg = "Action Done";
            return redirect(url('/dashboard/content/features'))->with('success',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $feature = Features::findOrFail($request->input('id'));
                if($feature->image){
                    if(file_exists("uploads/".$feature->image)){
                        unlink("uploads/".$feature->image);
                    }
                }
                
                $is_delete = Features::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/content/features'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }




    public function Achievements(Request $request){
        if($request->input('page')==''){
            $data['achievements'] =  Achievements::orderBy('sequence','asc')->get();
            return view('dashboard.content.achievement.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.content.achievement.create');
        }else if($request->input('page')=='save'){
             //save here
            $validate = $this->validate($request,[
                'title'     =>'required|min:1|max:50',
                'total'     =>'required|min:1|max:11',
                'sequence'     =>'required|min:1|max:2',
                'status'    =>'required'
            ]);
            $newAchievement = new Achievements();
            $newAchievement->title = $request->input('title');
            $newAchievement->total = $request->input('total');
            $newAchievement->sequence = $request->input('sequence');
            $newAchievement->status = $request->input('status');
            $newAchievement->created_by = Auth::user()->id;
            $newAchievement->save();
            $msg = "Achievement Added";
            return redirect(url('/dashboard/content/achievements'))->with('success',$msg);

        }else if($request->input('page')=='edit'){
            //edit view here
           
        }else if($request->input('page')=='update'){
            //update here
           
        }else if($request->input('page')=='status'){
            //update here
            $achievement = Achievements::findOrFail($request->input('id'));
            if($request->input('ref')=='Active'){
                $achievement->status = 'Inactive';
            }else{
                $achievement->status = 'Active';
            }
            $achievement->save();
            $msg = "Action Done";
            return redirect(url('/dashboard/content/achievements'))->with('success',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $is_delete = Achievements::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/content/achievements'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }
    


    public function Partners(Request $request){
        if($request->input('page')==''){
            $data['partners'] =  Partners::orderBy('sequence','asc')->get();
            return view('dashboard.content.partner.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.content.partner.create');
        }else if($request->input('page')=='save'){
             //save here
            $validate = $this->validate($request,[
                'partner_name'     =>'required|min:1|max:50',
                'status'    =>'required',
                'image'     => 'required|mimes:jpeg,png,jpg,JPG|max:5120',
                'sequence'     =>'required|min:1|max:2'
            ]);

            $newPartner = new Partners();
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newPartner->image = $newfilename;
            }
            
            $newPartner->partner_name = $request->input('partner_name');
            $newPartner->sequence = $request->input('sequence');
            $newPartner->status = $request->input('status');
            $newPartner->created_by = Auth::user()->id;
            $newPartner->save();
            $msg = "Partner Added";
            return redirect(url('/dashboard/content/partners'))->with('success',$msg);
        }else if($request->input('page')=='edit'){
            //edit view here
           
        }else if($request->input('page')=='update'){
            //update here
           
        }else if($request->input('page')=='status'){
            //update here
            $partner = Partners::findOrFail($request->input('id'));
            if($request->input('ref')=='Active'){
                $partner->status = 'Inactive';
            }else{
                $partner->status = 'Active';
            }
            $partner->save();
            $msg = "Action Done";
            return redirect(url('/dashboard/content/partners'))->with('success',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $partner = Partners::findOrFail($request->input('id'));
                if($partner->image){
                    if(file_exists("uploads/".$partner->image)){
                        unlink("uploads/".$partner->image);
                    }
                }
                $is_delete = Partners::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/content/partners'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }




    public function Testimonials(Request $request){
        if($request->input('page')==''){
            $data['testimonials'] =  Testimonials::orderBy('sequence','asc')->get();
            return view('dashboard.content.testimonial.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.content.testimonial.create');
        }else if($request->input('page')=='save'){
             //save here
            $validate = $this->validate($request,[
                'client_name'     =>'required|min:1|max:50',
                'body'     =>'required|min:1|max:300',
                'status'    =>'required',
                'image'     => 'required|mimes:jpeg,png,jpg,JPG | max:5120',
                'sequence'     =>'required|min:1|max:2'
            ]);

            $newTestimonial = new Testimonials();
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newTestimonial->image = $newfilename;
            }
            
            $newTestimonial->client_name = $request->input('client_name');
            $newTestimonial->body = $request->input('body');
            $newTestimonial->sequence = $request->input('sequence');
            $newTestimonial->status = $request->input('status');
            $newTestimonial->created_by = Auth::user()->id;
            $newTestimonial->save();
            $msg = "Testimonial Added";
            return redirect(url('/dashboard/content/testimonials'))->with('success',$msg);
        }else if($request->input('page')=='edit'){
            //edit view here
           
        }else if($request->input('page')=='update'){
            //update here
           
        }else if($request->input('page')=='status'){
            //update here
            $testimonial = Testimonials::findOrFail($request->input('id'));
            if($request->input('ref')=='Active'){
                $testimonial->status = 'Inactive';
            }else{
                $testimonial->status = 'Active';
            }
            $testimonial->save();
            $msg = "Action Done";
            return redirect(url('/dashboard/content/testimonials'))->with('success',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $testimonial = Testimonials::findOrFail($request->input('id'));
                if($testimonial->image){
                    if(file_exists("uploads/".$testimonial->image)){
                        unlink("uploads/".$testimonial->image);
                    }
                }
                $is_delete = Testimonials::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/content/testimonials'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }























}