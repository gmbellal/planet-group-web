<?php

namespace App\Http\Controllers\Dashboard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\Companies;




class CompanyController extends Controller{


    public function __construct(){
        //construct here


    }

    public function index(){
       
    }


    public function Companies(Request $request){
        if($request->input('page')==''){
            $data['companies'] =  Companies::orderBy('sequence','asc')->get();
            return view('dashboard.company.index',$data);
        }else if($request->input('page')=='create'){
            return view('dashboard.company.create');
        }else if($request->input('page')=='save'){
             //save here
            $validate = $this->validate($request,[
                'company_name'  =>'required|min:1|max:50',
                'body'           =>'required|min:5|max:5000',
                'url'           =>'required|min:4|max:50',
                'status'        =>'required',
                'image'         => 'required|mimes:jpeg,png,jpg,JPG | max:5120',
                'sequence'  =>'required|min:1|max:2'
            ]);

            $newCompany = new Companies();
            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $newCompany->image = $newfilename;
            }
            
            $newCompany->company_name = $request->input('company_name');
            $newCompany->url = $request->input('url');
            $newCompany->sequence = $request->input('sequence');
            $newCompany->status = $request->input('status');
            $newCompany->created_by = Auth::user()->id;
            $newCompany->save();
            
            $msg = "Company Added";
            return redirect(url('/dashboard/companies'))->with('success',$msg);

        }else if($request->input('page')=='edit'){
            $data['item'] = Companies::findOrFail($request->input('id'));
            //dd($data['item']);
            return view('dashboard.company.edit',$data);
        }
        
        
        else if($request->input('page')=='update'){
            //update here
            $validate = $this->validate($request,[
                'company_name'   =>'required|min:1|max:50',
                'body'           =>'required|min:5|max:5000',
                'url'            =>'required|min:4|max:50',
                'status'         =>'required',
                'image'          => 'mimes:jpeg,png,jpg,JPG | max:5120',
                'sequence'       =>'required|min:1|max:2'
            ]);

            $updateCompany = Companies::findOrFail($request->input('id'));

            if($request->hasFile('image')){
                $source_file = $request->file('image');
                $upload_path = 'uploads';
                if (!is_dir($upload_path)) {
                    mkdir($upload_path, 0777);
                    chmod($upload_path, 0777);
                }
                $newfilename = uniqid('',TRUE).'.'.$source_file->getClientOriginalExtension();
                $source_file->move($upload_path, $newfilename);
                chmod($upload_path.'/'.$newfilename, 0777);
                $updateCompany->image = $newfilename;
                $new_image = $newfilename;
            }

            $updateCompany->company_name = $request->input('company_name');
            $updateCompany->body = $request->input('body');
            $updateCompany->url = $request->input('url');
            $updateCompany->status = $request->input('status');
            $updateCompany->sequence = $request->input('sequence');
            $updateCompany->updated_by = Auth::user()->id;
            $is_update = $updateCompany->save();

           

            if($is_update){
                if(!empty($new_image)){
                    if(!empty($request->input('old_image')) && file_exists("uploads/".$request->input('old_image'))){
                        unlink("uploads/".$request->input('old_image'));
                    }
                }
                $msg = "Company Updated";
                return redirect(url('/dashboard/companies'))->with('success',$msg);
            }else{
                echo "Opps! Something went Wrong. Please try later";
                exit();
            }
            
        }
        
        
        else if($request->input('page')=='status'){
            //update here
            $company = Companies::findOrFail($request->input('id'));
            if($request->input('ref')=='Active'){
                $company->status = 'Inactive';
            }else{
                $company->status = 'Active';
            }
            $company->save();
            $msg = "Action Done";
            return redirect(url('/dashboard/companies'))->with('success',$msg);
        }else if($request->input('page')=='delete'){
            //delete here
            if($request->input('id')){
                $company = Companies::findOrFail($request->input('id'));
                if($company->image){
                    if(file_exists("uploads/".$company->image)){
                        unlink("uploads/".$company->image);
                    }
                }
                $is_delete = Companies::findOrFail($request->input('id'))->delete();
                $msg = "Delete Done";
                return redirect(url('/dashboard/companies'))->with('error',$msg);
            }
        }else{
            echo "Invalid Request";
            exit();
        }
    }







}