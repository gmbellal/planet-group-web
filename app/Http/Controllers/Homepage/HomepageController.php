<?php

namespace App\Http\Controllers\Homepage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Auth;
use App\Http\helpers;
use App\Model\Slider;
use App\Model\AboutUs;
use App\Model\ContactUs;
use App\Model\Services;
use App\Model\Features;
use App\Model\Achievements;
use App\Model\Partners;
use App\Model\Testimonials;
use App\Model\Companies;
use App\Model\Products;
use App\Model\Categories;
use App\Model\ContactForm;
use App\Model\Tracker;


class HomepageController extends Controller{
    public function __construct( Request $request){
        //do some stuff
        $agent = $request->server('HTTP_USER_AGENT');
        Tracker::hit($agent);
    }

    public function Index(){
        $data['title'] = 'Planet Group : Home';
        $data['slider'] =  Slider::orderBy('slider_sequence','asc')->where('status','Active')->get();
        $data['about'] =  AboutUs::get()->first();
        $data['services'] = Services::get()->first();
        $data['features'] = Features::orderBy('feature_sequence','asc')->where('status','Active')->get();
        $data['achievements'] = Achievements::orderBy('sequence','asc')->where('status','Active')->get();
        $data['partners'] = Partners::orderBy('sequence','asc')->where('status','Active')->get();
        $data['testimonials'] = Testimonials::orderBy('sequence','asc')->where('status','Active')->get();
        //dd($data);
        return view('homepage.home',$data);
    }

    
    public function About(){
        $data['title'] = 'Planet Group : About';
        $data['slider'] =  Slider::orderBy('slider_sequence','asc')->where('status','Active')->get();
        $data['about'] =  AboutUs::get()->first();
        $data['services'] = Services::get()->first();
        $data['features'] = Features::orderBy('feature_sequence','asc')->where('status','Active')->get();
        $data['achievements'] = Achievements::orderBy('sequence','asc')->where('status','Active')->get();
        $data['partners'] = Partners::orderBy('sequence','asc')->where('status','Active')->get();
        $data['testimonials'] = Testimonials::orderBy('sequence','asc')->where('status','Active')->get();
        //dd($data);
        return view('homepage.about',$data);
    }


    public function Contact(Request $request){
        $data['title'] = 'Planet Group : Contact';
        $data['contact'] =  ContactUs::get()->first();
        if($request->method() == 'GET'){
            return view('homepage.contact',$data);
        }else if($request->method() == 'POST'){
            $validate = $this->validate($request,[
                'name'          =>'required|min:5|max:50',
                'phone_number'  =>'required|min:10|max:15',
                'email'         =>'required|min:5|max:50',
                'subject'       =>'required|min:5|max:100',
                'message'      =>'required|min:5|max:500',

            ]);
            $ContactForm = new ContactForm();
            $ContactForm->name=$request->input('name');
            $ContactForm->phone_number=$request->input('phone_number');
            $ContactForm->email=$request->input('email');
            $ContactForm->subject=$request->input('subject');
            $ContactForm->message=$request->input('message');
            $is_save = $ContactForm->save();
            if($is_save){
                $msg = "Contact request submitted successfully";
                return redirect(url('/contact'))->with('success',$msg);
            }else{
                $msg = "Opps! Something went wrong, Try later...";
                return redirect(url('/contact'))->with('success',$msg);
            }
        }
        else{
            echo "Bad Request";
            exit();
        }
       
    }


    public function Company($url){
        $data['company'] =  Companies::where('url', $url)->first();
        $id = $data['company']->id;
        $data['categories'] =  Categories::where('company_id',$id)->get();
        $data['sub_cat'] =  Categories::where('parent_id','>', 0)->get();

        $data['products'] =  Products::where('company_id',$id)->get();
        $data['title'] = 'Planet Group : '.$data['company']->company_name;
        //dd($data); 
        return view('homepage.company',$data);
    }


    public function ProductCategory($id){
        $data['category'] =  Categories::findOrFail($id);
        $data['products'] =  Products::where('category_id',$id)->get();
        $data['title'] = 'Product catalog : '.$data['category']->category_name;
        return view('homepage.product_category',$data);
    }


    public function ProductDetails($id){
        
        $data['product'] =  Products::findOrFail($id);
        $data['title'] = 'Products : '.$data['product']->product_name;
        return view('homepage.product_details',$data);
    }


    
}
