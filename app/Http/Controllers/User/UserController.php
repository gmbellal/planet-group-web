<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\AppModule;
use App\Model\PermissionGroup;
use App\Model\AccGroupPermission;
use App\Model\Employee;
use App\Http\helpers;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use Carbon\Carbon;
use DateTime;
use Session;

class UserController extends Controller{


    public function __construct(){
        /*$module = 'user_management';
        $isPermit  = DB::table('acc_group_permission')->select('acc_group_permission.*', 'acc_app_module.name')->Where('acc_group_permission.group_role_id','=', Session::get('session_data')['role'])->Where('acc_app_module.name','=', $module)->leftjoin('acc_app_module', 'acc_app_module.id', '=', 'acc_group_permission.app_module_id')->get()->toArray();
        if($isPermit){
            if($isPermit[0]->view == false ){
                echo "You don't have permission for the selected action or page. Please contact with system admin.";
                exit();
            }
        }else{
            echo "You don't have permission for the selected action or page. Please contact with system admin.";
            exit();
        }*/
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users  = DB::table('acc_user_info')
            ->select('acc_user_info.*', 'os_employee.full_name', 'acc_group_role.role_name' )
            ->leftjoin('os_employee', 'os_employee.id', '=', 'acc_user_info.employee_id')
            ->leftjoin('acc_group_role', 'acc_group_role.role_id', '=', 'acc_user_info.role_id')
            ->get()->toArray();
        //dd($users);
        return view('user.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }
    public function view($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    


    public function permissionPage($id){
        $user_role = DB::table('acc_group_role')->get()->toArray();
        $user_info = DB::table('acc_user_info')->where('id',$id)->get()->toArray()[0];
        //dd($user_role);
      return view('user.user.permission',compact('user_info', 'user_role'));
    }


    public function permissionGiven(Request $request){
        $user_id =$request->input('user_id');
        $role_id =$request->input('role_id');

        $validate = $this->validate($request,[
            'user_id'      =>'required',
            'role_id'      =>'required'
        ]);
        //Update user role here..
        $is_update = DB::table('acc_user_info')
            ->where('id', $user_id)
            ->limit(1)
            ->update(array('role_id' => $role_id));

        if($is_update == true ){
            $request->session()->flash('failed','Failed');
            return redirect(route('user.index'));
        }else{
            $request->session()->flash('success','Updated User Role Information successfully!');
            return redirect(route('user.index'));
        }
    }


    public function permissionGroup(){
        $user_role = DB::table('acc_group_role')->get()->toArray();
        //dd($user_role);
        return view('user.group-permission.index',compact('user_role'));
    }

    public function addNewpermissionGroup(){
        return view('user.group-permission.create');
    }



    public function resetUserPassword(Request $request ,$id){
        if($request->method()== 'GET'){
            $user = User::select('*')->where('id', '=',$id)->first();
            return view('user.user.reset_user_password',compact('user'));
        }else{
            $validate = $this->validate($request,[
                'password' => 'min:6|required_with:verify_password|same:verify_password',
                'verify_password' => 'min:6'
            ]);
            $password = [
                "password" => md5($request->input('password'))
            ];
            $update=DB::table('acc_user_info')->where('id',$id)->update($password);
            return redirect(route('user.index'))->with('error','Password Reset Successfully.');
        } 
    }


    public function saveNewpermissionGroup(Request $request){

        $validate = $this->validate($request,[
            'role_name'      =>'required'
        ]);
        $NewPermissionGroup = [
            'role_name' => $request->input('role_name')
        ];
        $insert = PermissionGroup::create($NewPermissionGroup);

        if($insert== true){
            $request->session()->flash('success','Added New Group Permission successfully!');
            return redirect(url('group-permission'));
        }else{
            $request->session()->flash('failed','Submit Failed');
            return redirect(url('group-permission/add-new'));
        }
    }


    public function setGroupPermission($id){

        $app_module= DB::table('acc_app_module')
            ->select('acc_app_module.*', 'acc_group_permission.view', 'acc_group_permission.edit', 'acc_group_permission.recommendation', 'acc_group_permission.approve','acc_group_permission.report' )
            ->leftJoin('acc_group_permission',function ($join) use ($id) {
                $join->on( 'acc_group_permission.app_module_id', '=', 'acc_app_module.id');
                $join->where('acc_group_permission.group_role_id' , $id);
            })
            ->get()->toArray();
        $group_info = DB::table('acc_group_role')->where('role_id', $id)->get()->toArray()[0];

        return view('user.group-permission.set_group_permission',compact('app_module', 'group_info'));
    }

    
    public function saveGroupPermission(Request $request){
       $permission = $request->input('permission');
       $affectedRows = AccGroupPermission::where('group_role_id', $permission[0]['group_role_id'])->delete();
       AccGroupPermission::insert($permission);
       echo json_encode($permission);
    }




    public function passwordResetpage($id)
    {
        $user_data = getUserData($id);
        return view('user.user.create_user',compact('user_data','id'));
    }
    
    
    public function  updatePassword(Request $request,$id){
        $this->validate($request, [
            'password'         => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6'
        ]);
        $inputs = $request->except('_token','confirm_password');
        $inputs['password'] = md5($request->input('password'));

        if($id){
            $update =DB::table('acc_user_info')->where('employee_id',$id)->update($inputs);
            if($update){
                return redirect(route('login'))->with('success','Password Changed Successfully');
            }else{
                return redirect(route('login'))->with('error','Something went wrong');
            }
        }
    }

    
    


}
