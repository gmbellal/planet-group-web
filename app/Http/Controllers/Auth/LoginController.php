<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use App\Model\User;



class LoginController extends Controller{
    
    public function __construct(){
        $this->middleware('guest')->except('logout');
    }
    
    
    
    public function login(Request $request){
        if($request->method()=='GET'){
            return view('dashboard.auth.login');
        }
        else if($request->method()=='POST'){
            $this->validate($request, [
                'username'   => 'required',
                'password'    => 'required'
            ]);
            $user = User::where('username', $request->username)->where('password',md5($request->password))->first();
            if($user){
                Auth::login($user);
                $this->userSessionData();
                return redirect(url('dashboard'));
            }
            else{
                $request->session()->flash('error','Username/Password is invalid!');
                return redirect(route('login'));
            }
        }else{
            echo "Invalid Request !";
        }
    }



    public function userSessionData(){
        $session_data = array(
            'id'    => Auth::user()->id,
            'username'  => Auth::user()->username,
            'role_id'      =>  Auth::user()->role_id
        );
        Session::put('session_data',$session_data);
    }

    

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }



}
