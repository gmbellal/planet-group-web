<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
//use Excel;
if (! function_exists('sendSms')) {
    function sendSms($data){
        $url ='103.9.185.211/smsSendApi.php';
        $is_send= http_post($url,$data);

        $resultArray = json_decode($is_send,true);
        $results     = explode(',',$resultArray['result']);
        $sendStatus  = $results[0];

        if($sendStatus==200)
        {
            return ['success'=>1, 'message'=>'Message successfully sent','full_body'=>$is_send];
        }else{
            return ['success'=>0, 'message'=>$results[1],'full_body'=>$is_send,'error_code'=>$sendStatus];

        }
    }
}

if (! function_exists('http_post')) {
    function http_post($url, $data){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);

        curl_close($ch);
        return $output;
    }
}
if (!function_exists('http_get'))
{
    function http_get($url)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",

        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

}

if (!function_exists('getEnumValues'))
{
    function getEnumValues($table, $column)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }

}

if (!function_exists('ddx'))
{

    function ddx($data)
    {
        echo '<pre>', print_r($data), '</pre>', exit();
    }

}
if (!function_exists('get_view_date_time_format'))
{

    function get_view_date_time_format()
    {
        return 'M d \'y g:i A';
        //return 'd-m-Y g:i A';
    }

}

if (!function_exists('lastServiceId'))
{

    function lastServiceId($table_name,$column_name)
    {
        $lastServiceID = DB::table($table_name)->orderBy($column_name, 'DESC')->first();
        if($lastServiceID == "")
        {
            $lastServiceID = "1000";
            return $lastServiceID;
        }
        else
        {
            $lastServiceID = $lastServiceID->$column_name + 1;
            return $lastServiceID;
        }
    }

}
if (!function_exists('get_employee_list'))
{

    function get_employee_list($id)
    {
        $employee_name_list =[];
        $data['selected_employee_id'] =DB::table('requisition_items as rl')
                                          ->leftjoin('os_employee as emp', 'rl.employee_id', '=', 'emp.id')
                                          ->where('rl.requisition_id', '=',$id)
                                          ->select('emp.id','emp.full_name')
                                          ->get()->toArray();
        foreach ($data['selected_employee_id'] as $emp)
        {
            $employee_name_list[] = $emp->full_name;
        }
        return implode(',',$employee_name_list) ;
    }

}

if (!function_exists('driver_info'))
{

    function driver_info($driver_id)
    {
        $driver_name_mobile ='';
        $data['diver_info']    = DB::table('os_driver')->where('id',$driver_id)->get()->first();
        if(!empty($data['diver_info']))
        {
            $driver_name_mobile = 'Driver Name: '.$data['diver_info']->full_name . '<br> Contact No: '.$data['diver_info']->mobile_no .'';
        }
        return $driver_name_mobile;
    }

}

if (!function_exists('fromToDateCompare'))
{

    function fromToDateCompare($date1,$date2,$route_name)
    {
        if (Carbon::parse($date1)->gt($date2)){
            return redirect(".$route_name.")->with('error','To date cannot be less than from date!');
        }
    }

}
if (!function_exists('getDataByID'))
{

    function getDataByID($table_name,$field_name,$id)
    {
        return DB::table($table_name)->where($field_name,$id)->first();
    }

}
if (!function_exists('excelGenerate'))
{

    function excelGenerate($file_name,$sheet_name,$data)
    {
        Excel::create($file_name, function($excel) use( $data, $sheet_name) {
            $excel->sheet($sheet_name, function ($sheet) use($data){
                $sheet->fromArray($data,null,'A1',false,false);
            });
        })->download('xlsx');
    }

}
if (!function_exists('fileLocation'))
{
    $location=[];
    function fileLocation($location)
    {
        return $location;
    }

}
if (!function_exists('getUserData'))
{

    function getUserData(){
        $emp_data = DB::table('acc_users')
            ->where('acc_users.id', '=' , Auth::user()->id)
            ->select('acc_users.*')
            ->first();
        return  $emp_data;
    }

}
if (!function_exists('remindAt'))
{

    function remindAt($inputs)
    {
        $str ='-'.$inputs['day_to_remind'].' days';
        return date('Y-m-d H:i:s', strtotime($str, strtotime($inputs['expiry_date'])));
    }

}



function group_by($data, $group_by) {
    $result = [];
    foreach ($data as $key => $value) {
        $result[$value->$group_by][] = $value ;
    }
    return $result;
}

