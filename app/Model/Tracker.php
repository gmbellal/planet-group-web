<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tracker extends Model {
    
    public $attributes = [ 'hits' => -1 ];
    protected $fillable = [ 'ip', 'date', 'agent' ];
    protected $table = 'tracker';

    public static function boot() {
        // Any time the instance is updated (but not created)
        static::saving( function ($tracker) {
            $tracker->visit_time = date('H:i:s');
            $tracker->hits++;
        } );
    }

    public static function hit($agent) {

        static::firstOrCreate([
                  'ip'   => $_SERVER['REMOTE_ADDR'],
                  'date' => date('Y-m-d'),
                  'agent' => $agent,
              ])->save();
    }

}
