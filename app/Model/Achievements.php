<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Achievements extends Model{
    
    protected $table = 'achievements';
    protected $guarded =['id'];
}
