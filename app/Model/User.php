<?php
namespace App\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
class User extends Authenticatable{
    protected $table = 'acc_users';
    protected $primaryKey = 'id';
}

