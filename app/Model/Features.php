<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Features extends Model{
    
    protected $table = 'features';
    protected $guarded =['id'];
}
