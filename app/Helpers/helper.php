<?php
namespace App\Helpers;

class Helper{
    public static function regionList()
    {
        return array(
            '1' => 'Asia',
            '2' => 'Europe',
            '3' => 'America',
            '4' => 'Antarctica',
            '5' => 'Australia/Oceania',
            '6' => 'Africa',
        );
    }
}
